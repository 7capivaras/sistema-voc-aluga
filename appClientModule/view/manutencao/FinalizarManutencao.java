package view.manutencao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import view.Funcionalidade;
import view.Mascara;
import model.Manutencao;
import control.Controle;


@SuppressWarnings("serial")
public class FinalizarManutencao extends Funcionalidade {

	private static FinalizarManutencao finalizarManutencao = null;
	
	private JFormattedTextField jFTextPlaca;
	
	public FinalizarManutencao() {
		super("Devolver Carro em Manuten��o");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
			
		JLabel lblPlacaDoCarro = new JLabel("Placa do carro");
		lblPlacaDoCarro.setBounds(20, 139, 101, 14);
		add(lblPlacaDoCarro);
		
		jFTextPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		jFTextPlaca.setToolTipText("Digite a placa no seguite formato: PLC-1234");
		jFTextPlaca.setBounds(142, 136, 86, 20);
		this.add(jFTextPlaca);
		jFTextPlaca.setColumns(10);
		
		JButton btnFinalizar = new JButton("Finalizar Manuten��o");
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				finalizarManutencao();
			}
		});
		btnFinalizar.setBounds(238, 135, 170, 23);
		add(btnFinalizar);
		
	}
	
	protected void finalizarManutencao() {
		if (Controle.finalizarManutencao(jFTextPlaca.getText()))
			super.setMensagemConclusao("Devolu��o Feita");
		else
			super.setMensagemErro("Carro n�o est� em manuten��o ou n�o existe");
	}

	public static FinalizarManutencao getInstance() {
		
		if(finalizarManutencao == null){
			finalizarManutencao = new FinalizarManutencao();
		}
		
		return finalizarManutencao;
	}
	
	public void carregaFormulario(Manutencao m){
		this.jFTextPlaca.setText(m.getCarro().getPlaca());
	}
	
	public void apagaFormulario(){
		this.jFTextPlaca.setText("");
		super.limpaMensagem();
	}

}
