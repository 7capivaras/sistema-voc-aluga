package view;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import model.Funcionario;

import java.awt.Color;
import java.awt.Font;
import java.awt.FlowLayout;

@SuppressWarnings("serial")
public abstract class Funcionalidade extends JPanel {

	private JLabel lblAlerta;
	private JPanel componentAlerta;
	private String[] listaPermissoes;
	
	public static final String TODOS_PERMITIDOS = "todos";
	public static final String GERENTE_PERMITIDO = "gerente";
	public static final String AGENTE_PERMITIDO = "agente";
	
	public Funcionalidade(String nome) {
		this.setLayout(null);
		this.setSize(494, 347);
		
		JLabel lblTitulo = new JLabel(nome);
		lblTitulo.setFont(new Font("Arial", Font.BOLD, 27));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(23, 11, 432, 43);
		add(lblTitulo);
		
		componentAlerta = new JPanel();
		componentAlerta.setBackground(Color.DARK_GRAY);
		componentAlerta.setSize(this.getWidth(), 25);
		componentAlerta.setLocation(0, this.getHeight() - componentAlerta.getHeight());
		componentAlerta.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		add(componentAlerta);
		
		lblAlerta = new JLabel("");
		this.lblAlerta.setForeground(Color.BLACK);
		lblAlerta.setFont(new Font("Arial", Font.BOLD, 12));
		componentAlerta.add(lblAlerta);
		
		this.listaPermissoes = new String[]{Funcionalidade.TODOS_PERMITIDOS};

	}
	
	public void setPermissaoAcesso(String [] permitidos){
		this.listaPermissoes = permitidos;
	}
	
	public boolean verificaPermissao(Funcionario f){
		String permissao = f.getCargo();
		for(int i=0; i < this.listaPermissoes.length; i++){
			if( this.listaPermissoes[i].equals(Funcionalidade.TODOS_PERMITIDOS) )
				return true;
			
			if( this.listaPermissoes[i].equals(permissao))
				return true;
		}
		return false;
	}
	
	protected void limpaMensagem(){
		this.lblAlerta.setText("");
		this.componentAlerta.setBackground(Color.DARK_GRAY);
	}
	
	protected void setMensagemErro(String mensagem){
		this.lblAlerta.setText(mensagem);
		this.componentAlerta.setBackground(Color.RED);
	}
	
	protected void setMensagemAlerta(String mensagem){
		this.lblAlerta.setText(mensagem);
		this.componentAlerta.setBackground(Color.YELLOW);
		
	}
	
	protected void setMensagemProcessamento(String mensagem){
		this.lblAlerta.setText(mensagem);
		this.componentAlerta.setBackground(Color.BLUE);
	}
	
	protected void setMensagemConclusao(String mensagem){
		this.lblAlerta.setText(mensagem);
		this.componentAlerta.setBackground(Color.GREEN);
	}
	
	public abstract void apagaFormulario();
	public <T> void carregaFormulario(T t){}
}
