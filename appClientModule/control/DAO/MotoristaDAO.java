package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Locacao;
import model.Motorista;
import model.Util;
import control.ConnectionFactory;

public class MotoristaDAO {
	
	public static boolean salvarMotoristas(Locacao l){
		
		boolean concluido = false;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try{
			stmt = con.prepareStatement("INSERT INTO voce_aluga.motorista(" +
											"cnh, cpf, nome, locacao_id)" +
											"VALUES (?, ?, ?, ?);");
			
			for(Motorista m:l.getMotoristas()){
				stmt.setString(1, m.getCnh());
				stmt.setLong(2, m.getCpf());
				stmt.setString(3, m.getNome());
				stmt.setLong(4, l.getIdLocacao());
				
				stmt.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}

	public static ArrayList<Motorista> buscarRegistros(String cpf) {
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		long motoristacpf = Util.StringToLong(cpf);
		
		ArrayList<Motorista> motoristas = new ArrayList<Motorista>();
		Motorista motorista = null;
		
		try{
			stmt = con.prepareStatement("select * from voce_aluga.motorista where cpf = ?");
			stmt.setLong(1, motoristacpf);
			
			ResultSet resultset = stmt.executeQuery();
			while(resultset.next()){
				motorista = new Motorista(	resultset.getLong("id"), resultset.getLong("cpf"), 
											resultset.getString("cnh"), resultset.getString("nome"));
				motorista.setLocacao(LocacaoDAO.getLocacaoById(resultset.getLong("locacao_id")));
				motoristas.add(motorista);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return motoristas;
	}
	
	private static Motorista getmotorista(ResultSet resultset) {
		Motorista motorista = null;
		try{
			motorista = new Motorista(resultset.getLong("id"), resultset.getLong("cpf"), resultset.getString("cnh"), resultset.getString("nome"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return motorista;
	}

	public static Motorista getMotoristaById(long cpf) {
		Motorista motorista = null;

		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try{
			stmt = con.prepareStatement("select * from voce_aluga.motorista where cpf = ?");
			stmt.setLong(1, cpf);
			
			ResultSet resultset = stmt.executeQuery();
			while(resultset.next()){
				motorista = new Motorista(	resultset.getLong("id"), resultset.getLong("cpf"), 
											resultset.getString("cnh"), resultset.getString("nome"));
				motorista.setLocacao(LocacaoDAO.getLocacaoById(resultset.getLong("locacao_id")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return motorista;
	}
}
