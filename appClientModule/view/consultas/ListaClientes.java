package view.consultas;

import java.util.ArrayList;

import control.Controle;
import model.Cliente;

import javax.swing.JButton;

import view.Lista;
import view.cadastros.MantemCliente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaClientes extends Lista<Cliente> {
	
	private static ListaClientes listaClientes;
	private ArrayList<Cliente> clientes;

	private ListaClientes() {
		super("Lista de Clientes");
		String[] cabecalho = new String[]{"CPF","Nome"};
		super.setCabecalho(cabecalho);
		super.setModel();
		super.setSelecaoSigular();
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editar();
			}
		});
		btnEditar.setBounds(357, 291, 89, 23);
		add(btnEditar);
		
		JButton btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDados();
			}
		});
		btnCarregar.setBounds(229, 291, 89, 23);
		add(btnCarregar);

	}
	
	public static ListaClientes getInstance(){
		if(listaClientes == null){
			listaClientes = new ListaClientes();
		}
		
		return listaClientes;
	}
	
	private void setDados(){
		this.clientes = Controle.getClientes();
		super.carregaDados(this.clientes);
	}
	
	private void editar(){
		super.trocaTela(MantemCliente.getInstance());
	}
	
	protected String[] getTupla(Cliente c){
		String[] tupla = new String[]{c.getCpf()+"", c.getNome()};
		return tupla;
	}
}
