package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import control.ConnectionFactory;
import model.Autorizacao;
import model.Util;

public class AutorizacaoDAO {

	public static boolean registraRetirada(Autorizacao autorizacao) {

		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		boolean concluido = false;
		
		try {
			stmt = con.prepareStatement("INSERT INTO voce_aluga.autorizacao(" +
						"idmotorista,idlocacao)"+
						" VALUES (?, ?);");
			
			stmt.setLong(1, autorizacao.getMotorista().getId());
			stmt.setLong(2, autorizacao.getLocacao().getIdLocacao());

			stmt.execute();
			concluido = true;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;		
	}

}
