package test;

import java.util.Calendar;

import junit.framework.*;

import org.junit.Test;

import model.Util;

public class UtilTest extends TestCase{

	@Test
	public void testValidarCPF() {
		long cpf1 = 12345678909L;
		long cpf2 = 12345678910L;
		
		boolean resposta1 = Util.validarCPF(cpf1);
		boolean resposta2 = Util.validarCPF(cpf2);
		
		assertEquals("CPF v�lido, retorna true",true,resposta1);
		assertEquals("CPF inv�lido, retorna false",false,resposta2);
	}
	
	public void testCalendarToString(){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 10);
		c.set(Calendar.MONTH, 9);
		c.set(Calendar.YEAR, 2010);
		
		String strdata = Util.calendarToString(c);
		
		assertEquals("Estrutura da data correta, retorna true","10/10/2010",strdata);
		
		c.set(Calendar.DAY_OF_MONTH, 8);
		c.set(Calendar.MONTH, 8);
		c.set(Calendar.YEAR, 2010);
		
		strdata = Util.calendarToString(c);

		assertEquals("Estrutura da data correta, retorna true","08/09/2010",strdata);
	}
}
