package view.consultas;

import java.util.ArrayList;

import control.Controle;
import model.Reserva;
import model.Util;

import javax.swing.JButton;

import view.Lista;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaReservas extends Lista<Reserva>{

	private static ListaReservas listaReservas = null;
	private ArrayList<Reserva> reservas;
	
	public ListaReservas() {
		super("Lista de Reservas");
		String[] cabecalho = new String[]{ 	"C�digo Reserva",
				"Nome Cliente", 
				"Placa Carro", 
				"Data da Retirada" };
		super.setCabecalho(cabecalho);
		super.setModel();
		super.setSelecaoSigular();
		
		JButton btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDados();
			}
		});
		btnCarregar.setBounds(229, 291, 89, 23);
		add(btnCarregar);
	}
	
	public static ListaReservas getInstance(){
		if(listaReservas == null){
			listaReservas = new ListaReservas();
		}
		
		return listaReservas;
	}

	private void setDados() {
		this.reservas = Controle.getReservas();
		super.carregaDados(reservas);
	}

	protected String[] getTupla(Reserva r){
		String[] tupla = new String[]{ 	r.getIdReserva()+"", 
										r.getCliente().getNome(), 
										r.getCarro().getPlaca(),
										Util.calendarToString(r.getDataRetirada()) };
		return tupla;
	}
}