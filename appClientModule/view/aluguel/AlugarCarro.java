package view.aluguel;

import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.JButton;

import view.Funcionalidade;
import view.Mascara;
import view.Principal;
import model.Carro;
import model.Locacao;
import model.Reserva;
import model.Util;
import model.Motorista;

import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import control.Controle;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

@SuppressWarnings("serial")
public class AlugarCarro extends Funcionalidade {
	
	private static AlugarCarro alugarCarro = null;
	private JTextField textFieldNomeCartao;

	private JFormattedTextField jFTextNumeroCartao;
	private JFormattedTextField jFTextCPF;
	private JFormattedTextField jFTextValidadeCartao;
	private JFormattedTextField jFTextPrevisaoEntrega;
	private JFormattedTextField jFTextDataRetirada;
	private JFormattedTextField jFTextPlaca;
	private JFormattedTextField formattedTextFieldCRC;
	
	private Reserva reserva;
	private ArrayList<Motorista> motoristas;

	private AlugarCarro() {
		super("Aluguel");
		
		JLabel lblCpfDoCliente = new JLabel("CPF do cliente");
		lblCpfDoCliente.setBounds(10, 88, 80, 14);
		add(lblCpfDoCliente);
		
		jFTextCPF = new JFormattedTextField(Mascara.mascaraCPF());
		jFTextCPF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if( !jFTextCPF.getText().replaceAll("[-._]", "").equals("") ){
					verificarCPF();
				}
			}
		});
		jFTextCPF.setBounds(146, 85, 105, 20);
		add(jFTextCPF);
		
		JButton btnBuscarReserva = new JButton("Buscar Reserva");
		btnBuscarReserva.setBounds(279, 81, 189, 23);
		btnBuscarReserva.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				SelecionaReserva.getInstance().escolheReserva(AlugarCarro.getInstance());
			}
		});
		add(btnBuscarReserva);	
		
		JLabel lblNewLabel = new JLabel("Placa do carro");
		lblNewLabel.setBounds(10, 124, 107, 14);
		add(lblNewLabel);
		
		jFTextPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		jFTextPlaca.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if( !jFTextPlaca.getText().replaceAll("[-_]", "").equals("") 
						&& !jFTextDataRetirada.getText().replaceAll("[/_]", "").equals("")
						&& !jFTextPrevisaoEntrega.getText().replaceAll("[/_]", "").equals("")){
					verificaDisponibilidadeCarro();
				}
			}
		});
		jFTextPlaca.setToolTipText("Digite a placa no seguite formato: PLC-1234");
		jFTextPlaca.setBounds(146, 121, 86, 20);
		add(jFTextPlaca);
		
		JLabel lblPrevisaoEntrega = new JLabel("Previs\u00E3o de entrega");
		lblPrevisaoEntrega.setBounds(10, 187, 126, 14);
		add(lblPrevisaoEntrega);
		
		jFTextPrevisaoEntrega = new JFormattedTextField(Mascara.mascaraData());
		jFTextPrevisaoEntrega.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if( !jFTextPlaca.getText().replaceAll("[-_]", "").equals("") 
						&& !jFTextDataRetirada.getText().replaceAll("[/_]", "").equals("")
						&& !jFTextPrevisaoEntrega.getText().replaceAll("[/_]", "").equals("")){
					verificaDisponibilidadeCarro();
				}
			}
		});
		jFTextPrevisaoEntrega.setToolTipText("Digite a data no seguinte formato: dd/mm/aaaa");
		jFTextPrevisaoEntrega.setBounds(146, 184, 89, 20);
		add(jFTextPrevisaoEntrega);
		jFTextPrevisaoEntrega.setColumns(10);
		
		JLabel lblNumeroDoCarto = new JLabel("Cart\u00E3o de cr\u00E9dito");
		lblNumeroDoCarto.setBounds(6, 218, 130, 14);
		add(lblNumeroDoCarto);
		
		jFTextNumeroCartao = new JFormattedTextField(Mascara.mascaraCartao());
		jFTextNumeroCartao.setBounds(146, 215, 147, 20);
		add(jFTextNumeroCartao);
		jFTextNumeroCartao.setColumns(10);
		
		JLabel lblNomeNoCarto = new JLabel("Nome no cart\u00E3o");
		lblNomeNoCarto.setBounds(10, 249, 107, 14);
		add(lblNomeNoCarto);
		
		textFieldNomeCartao = new JTextField();
		textFieldNomeCartao.setBounds(146, 246, 147, 20);
		add(textFieldNomeCartao);
		textFieldNomeCartao.setColumns(10);
		
		JLabel lblValidadeDoCarto = new JLabel("Validade do cart\u00E3o");
		lblValidadeDoCarto.setBounds(10, 280, 130, 14);
		add(lblValidadeDoCarto);
		
		jFTextValidadeCartao = new JFormattedTextField(Mascara.mascaraValidadeCartao());
		jFTextValidadeCartao.setBounds(146, 277, 89, 20);
		add(jFTextValidadeCartao);
		jFTextValidadeCartao.setColumns(10);
		
		JLabel lblCrcDoCartao = new JLabel("CRC");
		lblCrcDoCartao.setBounds(258, 277, 35, 20);
		add(lblCrcDoCartao);
		
		JButton btnAlugar = new JButton("Alugar");
		btnAlugar.setBounds(379, 273, 89, 23);
		btnAlugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				alugar();
			}
		});
		add(btnAlugar);
		
		formattedTextFieldCRC = new JFormattedTextField(Mascara.mascaraCRC());
		formattedTextFieldCRC.setColumns(10);
		formattedTextFieldCRC.setBounds(303, 277, 35, 20);
		add(formattedTextFieldCRC);
		
		JLabel lblDataDeRetirada = new JLabel("Data de retirada");
		lblDataDeRetirada.setBounds(10, 156, 107, 14);
		add(lblDataDeRetirada);
		
		jFTextDataRetirada = new JFormattedTextField(Mascara.mascaraData());
		jFTextDataRetirada.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if( !jFTextPlaca.getText().replaceAll("[-_]", "").equals("") 
						&& !jFTextDataRetirada.getText().replaceAll("[/_]", "").equals("")
						&& !jFTextPrevisaoEntrega.getText().replaceAll("[/_]", "").equals("")){
					verificaDisponibilidadeCarro();
				}
			}
		});
		jFTextDataRetirada.setBounds(146, 153, 86, 20);
		add(jFTextDataRetirada);
		
		JButton btnBuscarCarro = new JButton("Buscar Carro");
		btnBuscarCarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SelecionaCarro.getInstance().escolheCarro(AlugarCarro.getInstance());
			}
		});
		btnBuscarCarro.setBounds(279, 117, 189, 23);
		add(btnBuscarCarro);
		
		JButton btnMotoristasAutorizados = new JButton("Motoristas Autorizados");
		btnMotoristasAutorizados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				motoristasAutorizados();
			}
		});
		btnMotoristasAutorizados.setBounds(279, 155, 189, 23);
		add(btnMotoristasAutorizados);

	}

	public static AlugarCarro getInstance(){
		if(alugarCarro == null){
			alugarCarro = new AlugarCarro();
		}
		
		return alugarCarro;
	}
	
	private void verificarCPF() {
		
		if( Controle.verificarClienteCPF(this.jFTextCPF.getText()) ){
			super.setMensagemConclusao("Cliente encontrado");	
		}else{
			super.setMensagemErro("Cliente n�o encontrado!");
		}
		
	}
	
	private void verificaDisponibilidadeCarro() {
		
		if( Controle.verificaDisponibilidadeCarro(this.jFTextPlaca.getText(), this.jFTextDataRetirada.getText(), this.jFTextPrevisaoEntrega.getText())){
			super.setMensagemConclusao("Carro dispon�vel");
		}else{
			super.setMensagemErro("Indispon�vel!");
		}		
	}
	
	private void alugar() {
		if( this.motoristas == null ){
			super.setMensagemAlerta("Nenhum motorista foi adicionado � loca��o!");
			return;
		}
		if( this.motoristas.size() == 0){
			super.setMensagemAlerta("Nenhum motorista foi adicionado � loca��o!");
			return;
		}
		
		long idReserva = 0;
		if( this.reserva != null )
			idReserva = this.reserva.getIdReserva();
		
		if (Controle.alugarCarro(this.jFTextCPF.getText(), this.jFTextPlaca.getText(), this.jFTextPrevisaoEntrega.getText(), 
				this.jFTextDataRetirada.getText(), this.jFTextNumeroCartao.getText().replaceAll("[- ]", ""), this.textFieldNomeCartao.getText(), 
				this.jFTextValidadeCartao.getText(), this.formattedTextFieldCRC.getText(), idReserva)){
			
			super.setMensagemConclusao("Loca��o feita com sucesso");
			salvaMotoristas();
			
		}else{
			super.setMensagemErro("Erro na loca��o!");
		}	
	}
	
	private void salvaMotoristas() {
		if( this.motoristas == null )
			return;
		if( this.motoristas.size() == 0)
			return;
		
		Locacao locacao = Controle.getUltimaLocacao();
		for(Motorista m:this.motoristas){
			locacao.addMotorista(m);
		}
		
		Controle.registraMotoristas(locacao);
	}

	public void apagaFormulario(){
		this.jFTextNumeroCartao.setText("");
		this.textFieldNomeCartao.setText("");
		
		this.jFTextCPF.setText("");
		this.jFTextValidadeCartao.setText("");
		this.jFTextPrevisaoEntrega.setText("");
		this.jFTextDataRetirada.setText("");
		this.jFTextPlaca.setText("");
		this.formattedTextFieldCRC.setText("");
		super.limpaMensagem();
	}

	public <T> void carregaFormulario(T t) {
		if(t instanceof Carro){
			Carro carro = (Carro)t;
			this.jFTextPlaca.setText(carro.getPlaca());
		}
		if(t instanceof Reserva){
			Reserva r = (Reserva)t;
			this.reserva = r;
			this.jFTextPlaca.setText(reserva.getCarro().getPlaca());
			this.jFTextCPF.setText( reserva.getCliente().getCpf()+"" );
			this.jFTextDataRetirada.setText(Util.calendarToString(reserva.getDataRetirada()));
			this.jFTextPrevisaoEntrega.setText(Util.calendarToString(reserva.getValidadeReserva()));
		}
	}
	
	public void carregaMotoristas(ArrayList<Motorista> ms){
		if(ms == null)
			return;
		
		if(ms.size() != 0){
			this.motoristas = ms;
			super.setMensagemConclusao("Motoristas Adicionados");
		}
	}
	
	private void motoristasAutorizados() {
		Principal.getInstance().trocaPanel(AdicionaMotoristas.getInstance());
		AdicionaMotoristas.getInstance().carregaDados(this.motoristas);
	}
}
