package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import model.Util;
import control.ConnectionFactory;

public class VendaDAO {

	public static boolean venderCarro(long carro_id, long cliente_id, long preco) {

		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		boolean concluido = false;
		
		java.sql.Date datavenda = Util.calendarToSQL(Calendar.getInstance());
		
		try {
			stmt = con.prepareStatement("INSERT INTO voce_aluga.venda(" +
						"datavenda, valor, carro_id, " +
						"cliente_id) VALUES (?, ?, ?, ?);");
			
			stmt.setDate(1, datavenda);
			stmt.setLong(2, preco);
			stmt.setLong(3, carro_id);
			stmt.setLong(4, cliente_id);

			stmt.execute();
			concluido = true;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}
}
