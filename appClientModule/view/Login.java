package view;

import model.*;
import control.*;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFormattedTextField;

@SuppressWarnings("serial")
public class Login extends Funcionalidade{

	private static Login login = null;
	
	private JFormattedTextField formattedTextFieldCPF;
	private JPasswordField passwordField;
	
	private Funcionario funcionario;
	
	private Login() {
		super("Voc\u00EA Aluga");
		super.setBounds(0, 23, super.getWidth(), super.getHeight());;
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCpf.setBounds(55, 122, 43, 24);
		this.add(lblCpf);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenha.setBounds(55, 170, 43, 24);
		this.add(lblSenha);
		
		passwordField = new JPasswordField();
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER )
					conectar();
			}
		});
		passwordField.setBounds(159, 174, 161, 20);
		this.add(passwordField);
		
		JButton btnConectar = new JButton("Conectar");
		btnConectar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				conectar();
			}
		});
		btnConectar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER )
					conectar();
			}
		});
		btnConectar.setBounds(332, 247, 89, 23);
		this.add(btnConectar);
		
		formattedTextFieldCPF = new JFormattedTextField(Mascara.mascaraCPF());
		formattedTextFieldCPF.setColumns(14);
		formattedTextFieldCPF.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER )
					conectar();
			}
		});
		formattedTextFieldCPF.setBounds(159, 126, 161, 20);
		this.add(formattedTextFieldCPF);
	}
	
	public static Login getInstance(){
		if(login == null){
			login = new Login();
		}
		
		return login;
	}
	public void conectar(){
		char[] temps = this.passwordField.getPassword();
		String senha = new String(temps);
		long CPF = Long.parseLong( this.formattedTextFieldCPF.getText().replaceAll("[_.-]", "") );
		
		if(Util.validarCPF(CPF)){
			super.setMensagemProcessamento("Acessando o banco de dados...");
			this.funcionario = Controle.conectarSistema(CPF, senha);
			if( this.funcionario != null ){
				super.limpaMensagem();
				this.formattedTextFieldCPF.setText("");
				this.passwordField.setText("");
				Principal.getInstance().initPrincipal();
			}
			else{
				super.setMensagemErro("CPF ou senha est�o incorretos!");
			}
		}
		else
			super.setMensagemErro("CPF inv�lido");
		
		
	}
	
	public Funcionario getFuncionario(){
		return this.funcionario;
	}

	@Override
	public void apagaFormulario() {
		// TODO Auto-generated method stub
		
	}
}
