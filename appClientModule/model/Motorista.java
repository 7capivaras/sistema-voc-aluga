package model;

public class Motorista
{
	private long id;
	private String cnh;
	private String nome;
	private long cpf;

	private Locacao locacao;

	public Motorista(	long id,
						long cpf,
						String cnh,
						String nome )
	{
		this.id = id;
		this.cpf = cpf;
		this.cnh = cnh;
		this.nome = nome;
		
		this.locacao = null;
	}

	public long getId() {
		return id;
	}

	public Locacao getLocacao() {
		return locacao;
	}

	public void setLocacao(Locacao locacao) {
		this.locacao = locacao;
	}

	public String getCnh() {
		return cnh;
	}

	public String getNome() {
		return nome;
	}

	public long getCpf() {
		return cpf;
	}
}

