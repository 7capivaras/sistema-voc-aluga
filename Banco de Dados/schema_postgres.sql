﻿drop schema voce_aluga cascade;
create schema voce_aluga;

create table voce_aluga.Agencia (
    idAgencia serial not null,
    nome varchar(255),
    capacidadeCarros integer not null,
    endereco varchar(255) not null,
    primary key (idAgencia)
);

create table voce_aluga.Autorizacao (
    idMotorista bigint not null,
    idLocacao bigint not null,
    primary key (idMotorista, idLocacao)
);

create table voce_aluga.Carro (
    id serial not null,
    chassi varchar(255) unique not null,
    dataAquisicao date not null,
    descricao varchar(255) not null,
    kmRodados real not null,
    marca varchar(255) not null,
    modelo varchar(255) not null,
    placa varchar(255) not null,
    ultimaRevisao date not null,
    valorDiaria real not null,
    agencia_id bigint not null,
    primary key (id)
);

create table voce_aluga.CartaoCredito (
    id serial not null,
    crc integer not null,
    nome varchar(255) not null,
    numero bigint not null,
    validade date not null,
    primary key (id)
);

create table voce_aluga.Cliente (
    id serial not null,
    cpf bigint unique not null,
    endereco varchar(255) not null,
    nascimento date not null,
    nome varchar(255) not null,
    telefone varchar(255) not null,
    primary key (id)
);

create table voce_aluga.Funcionario (
    id serial not null,
    cargo varchar(255) not null,
    cpf bigint unique not null,
    endereco varchar(255) not null,
    nome varchar(255) not null,
    senhaSistema varchar(255) not null,
    telefone varchar(255) not null,
    agencia_id bigint not null,
    primary key (id)
);

create table voce_aluga.Locacao (
    idLocacao serial not null,
    dataLocacao date not null,
    dataEntrega date,
    previsaoEntrega date not null,
    dataRetirada date not null,
    agencia_id bigint not null,
    cartaoCredito_id bigint not null,
    cliente_id bigint not null,
    carro_id bigint not null,
    primary key (idLocacao)
);

create table voce_aluga.Manutencao (
    idManutencao serial not null,
    dataAgendada date not null,
    dataDevolucao date,	
    motivoManutencao varchar(255) not null,
    tipoServico varchar(255) not null,
    carro_id bigint,
    funcionario_id bigint not null,
    primary key (idManutencao)
);

create table voce_aluga.Motorista (
    id serial not null,
    cnh varchar(255) unique not null,
    cpf bigint unique not null,
    nome varchar(255) not null,
    locacao_id bigint not null,
    primary key (id)
);

create table voce_aluga.Pagamento (
    idPagamento serial not null,
    dataPagamento date,
    formaPagamento varchar(255) not null,
    taxaAtraso real not null,
    taxaDanificacao real not null,
    taxaRetorno real not null,
    desconto real not null,
    valor real not null,
    locacao_id bigint not null,
    primary key (idPagamento)
);

create table voce_aluga.Reserva (
    idReserva serial not null,
    dataReserva date not null,
    dataRetirada date not null,
    validadeReserva date not null,
    carro_id bigint,
    cliente_id bigint not null,
    funcionario_id bigint not null,
    primary key (idReserva)
);

create table voce_aluga.Venda (
    idVenda serial not null,
    dataVenda date not null,
    valor real not null,
    carro_id bigint not null,
    cliente_id bigint not null,
    primary key (idVenda)
);

alter table voce_aluga.Autorizacao
	add constraint idMotorista_motorista_fk
	foreign key (idMotorista)
	references voce_aluga.Motorista;

alter table voce_aluga.Autorizacao
	add constraint idLocacao_locacao_fk
	foreign key (idLocacao)
	references voce_aluga.Locacao;

alter table voce_aluga.Carro 
    add constraint agencia_id_carro_fk 
    foreign key (agencia_id) 
    references voce_aluga.Agencia;

alter table voce_aluga.Funcionario 
    add constraint agencia_id_funcionario_fk 
    foreign key (agencia_id) 
    references voce_aluga.Agencia;

alter table voce_aluga.Locacao 
    add constraint cartaoCredito_id_locacao_fk 
    foreign key (cartaoCredito_id) 
    references voce_aluga.CartaoCredito;

alter table voce_aluga.Locacao 
    add constraint cliente_id_locacao_fk 
    foreign key (cliente_id) 
    references voce_aluga.Cliente;

alter table voce_aluga.Locacao 
    add constraint agencia_id_locacao_fk 
    foreign key (agencia_id) 
    references voce_aluga.Agencia;

alter table voce_aluga.Locacao 
    add constraint carro_id_carro_fk 
    foreign key (carro_id) 
    references voce_aluga.Carro;

alter table voce_aluga.Manutencao 
    add constraint funcionario_id_manutencao_fk 
    foreign key (funcionario_id) 
    references voce_aluga.Funcionario;

alter table voce_aluga.Manutencao 
    add constraint carro_id_manutencao_fk 
    foreign key (carro_id) 
    references voce_aluga.Carro;

alter table voce_aluga.Motorista 
    add constraint locacao_id_motorista_fk 
    foreign key (locacao_id) 
    references voce_aluga.Locacao;

alter table voce_aluga.Pagamento 
    add constraint locacao_id_pagamento_fk 
    foreign key (locacao_id) 
    references voce_aluga.Locacao;

alter table voce_aluga.Reserva 
    add constraint funcionario_id_reserva_fk 
    foreign key (funcionario_id) 
    references voce_aluga.Funcionario;

alter table voce_aluga.Reserva 
    add constraint cliente_id_reserva_fk 
    foreign key (cliente_id) 
    references voce_aluga.Cliente;

alter table voce_aluga.Reserva 
    add constraint carro_id_reserva_fk 
    foreign key (carro_id) 
    references voce_aluga.Carro;

alter table voce_aluga.Venda
    add constraint carro_id_venda_fk
    foreign key (carro_id)
    references voce_aluga.Carro;

alter table voce_aluga.Venda
    add constraint cliente_id_venda_fk
    foreign key (cliente_id)
    references voce_aluga.Cliente;


-------------------------------------Funçoes--------------------------------------------------

create or replace function voce_aluga.locacoesEmAberto(id bigint)
returns setof voce_aluga.locacao as
$$
begin
	return query select l.* from voce_aluga.locacao as l
		left outer join voce_aluga.pagamento as p
		on l.idlocacao = p.locacao_id
		where l.cliente_id = id and ( l.dataentrega is null or p.locacao_id is null )
		group by idLocacao;
end;
$$ language plpgsql;