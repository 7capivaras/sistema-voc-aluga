package view.manutencao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import view.Funcionalidade;
import view.Login;
import view.Mascara;
import model.Funcionario;
import control.Controle;

@SuppressWarnings("serial")
public class AgendarManutencao extends Funcionalidade {
	
	private static AgendarManutencao agendarManutencao = null;
	private JTextField textFieldTipoServico;
	private JTextField textFieldMotivo;
	private JFormattedTextField jFTextPlaca;
	private JFormattedTextField jFTextDataManutencao;

	private Funcionario funcionario;
	
	private AgendarManutencao() {
		super("Agendar Manuten\u00E7\u00E3o");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		JLabel lblPlacaDoCarro = new JLabel("Placa do carro");
		lblPlacaDoCarro.setBounds(10, 85, 130, 14);
		add(lblPlacaDoCarro);
		
		jFTextPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		jFTextPlaca.setToolTipText("Digite a placa no seguite formato: PLC-1234");
		jFTextPlaca.setBounds(154, 82, 86, 20);
		this.add(jFTextPlaca);
		jFTextPlaca.setColumns(10);
		
		
		JLabel lblDataDaManuteno = new JLabel("Data da manuten\u00E7\u00E3o");
		lblDataDaManuteno.setBounds(10, 125, 130, 14);
		add(lblDataDaManuteno);		
		
		jFTextDataManutencao = new JFormattedTextField(Mascara.mascaraData());
		jFTextDataManutencao.setBounds(154, 122, 86, 20);
		jFTextDataManutencao.setToolTipText("Digite a data no seguinte formato: dd/mm/aaaa");
		this.add(jFTextDataManutencao);
		
		JLabel lblTipoDeServio = new JLabel("Tipo de servi\u00E7o");
		lblTipoDeServio.setBounds(10, 172, 130, 14);
		add(lblTipoDeServio);
		
		textFieldTipoServico = new JTextField();
		textFieldTipoServico.setBounds(154, 169, 148, 20);
		add(textFieldTipoServico);
		textFieldTipoServico.setColumns(10);
		
		JLabel lblMotivoDaManuteno = new JLabel("Motivo da manuten\u00E7\u00E3o");
		lblMotivoDaManuteno.setBounds(10, 220, 130, 14);
		add(lblMotivoDaManuteno);
		
		textFieldMotivo = new JTextField();
		textFieldMotivo.setBounds(154, 217, 295, 20);
		add(textFieldMotivo);
		textFieldMotivo.setColumns(10);
		
		JButton btnAgendar = new JButton("Agendar");
		btnAgendar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agendar();
			}
		});
		btnAgendar.setBounds(360, 270, 89, 23);
		add(btnAgendar);
		
		JButton btnVerificar = new JButton("Verificar");
		btnVerificar.setBounds(272, 81, 89, 23);
		add(btnVerificar);

	}

	public static AgendarManutencao getInstance(){
		if(agendarManutencao == null){
			agendarManutencao = new AgendarManutencao();
		}
		
		return agendarManutencao;
	}
	
	public void apagaFormulario(){
		this.jFTextPlaca.setText("");
		this.jFTextDataManutencao.setText("");
		this.textFieldTipoServico.setText("");
		this.textFieldMotivo.setText("");
		super.limpaMensagem();
	}
	
	private void agendar() {
		
		this.funcionario = Login.getInstance().getFuncionario();
		
		if (Controle.agendarManutencao(jFTextPlaca.getText(), jFTextDataManutencao.getText(), textFieldTipoServico.getText(),
				textFieldMotivo.getText(), this.funcionario.getId()))
			super.setMensagemConclusao("Manutencao Agendada com Sucesso");
		else
			super.setMensagemErro("Erro no Agendamento");
		
	}
}
