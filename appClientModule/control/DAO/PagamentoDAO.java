package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import control.ConnectionFactory;
import model.Locacao;
import model.Util;
import model.Pagamento;


public class PagamentoDAO {
	

	public static boolean isListaNegra(long idCliente) {
			
		java.sql.Date dataAtual = Util.calendarToSQL(Calendar.getInstance());
		ArrayList<Locacao> locacoes = LocacaoDAO.buscarLocacoesAntigas(idCliente);
		
		boolean isListaNegra = true;
		
		if (locacoes== null || locacoes.isEmpty())
			return false;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
				
			for(Locacao locacao: locacoes){
				
				stmt = con.prepareStatement("select idpagamento from voce_aluga.Pagamento where locacao_id = ? and datapagamento <= ?");
				stmt.setLong(1, locacao.getIdLocacao());
				stmt.setDate(2, dataAtual);
				ResultSet resultSet = stmt.executeQuery();
				
				if( resultSet.next() ){
					isListaNegra = false;
				}		
				
			}	
		
		}catch (SQLException e) {		
			e.printStackTrace();
			isListaNegra = false;
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return isListaNegra;
	}
	
	public static boolean registrarPagamento(Pagamento p){
		
		boolean concluido = false;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try{
			stmt = con.prepareStatement("INSERT INTO voce_aluga.pagamento("+
            "datapagamento, formapagamento, taxaatraso, taxadanificacao, "+
            "taxaretorno, desconto, valor, locacao_id)"+
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
			
			stmt.setDate(1, Util.calendarToSQL(p.getDataPagamento()));
			stmt.setString(2, p.getFormaPagamento());
			stmt.setDouble(3, p.getTaxaAtraso());
			stmt.setDouble(4, p.getTaxaDanificacao());
			stmt.setDouble(5, p.getTaxaRetorno());
			stmt.setDouble(6, p.getDesconto());
			stmt.setDouble(7, p.getValor());
			stmt.setLong(8, p.getLocacao().getIdLocacao());
			stmt.execute();
			stmt.close();
			concluido = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}
	
	public ArrayList<Pagamento> getPendencias(){
		ArrayList<Pagamento> pendencias = new ArrayList<Pagamento>();
		Pagamento pagamento = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.pagamento where datapagamento is null;");
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				pagamento = getPagamento(resultSet);
				pendencias.add(pagamento);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(pendencias.size() == 0){
			return null;
		}
		
		return pendencias;
	}
	
	public static Locacao getPagamentosLocacao(Locacao l){
		Pagamento pagamento = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.pagamento where locacao_id = ?;");
			stmt.setLong(1, l.getIdLocacao());
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				pagamento = getPagamento(resultSet,l);
				l.addPagamento(pagamento);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return l;
	}
	
	private static Pagamento getPagamento(ResultSet resultSet, Locacao l) {
		Pagamento pagamento = null;
		
		try{
			pagamento = new Pagamento(	resultSet.getLong("idpagamento"),
										resultSet.getDouble("valor"),
										Util.stringSQLToCalendar(resultSet.getString("datapagamento")),
										resultSet.getDouble("taxaretorno"),
										resultSet.getDouble("taxadanificacao"),
										resultSet.getDouble("taxaatraso"),
										resultSet.getDouble("desconto"),
										resultSet.getString("formapagamento") );
			
			pagamento.setLocacao(l);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pagamento;
	}

	private static Pagamento getPagamento(ResultSet resultSet){
		
		Pagamento pagamento = null;
		
		try{
			pagamento = new Pagamento(	resultSet.getLong("idpagamento"),
										resultSet.getDouble("valor"),
										Util.stringSQLToCalendar(resultSet.getString("datapagamento")),
										resultSet.getDouble("taxaretorno"),
										resultSet.getDouble("taxadanificacao"),
										resultSet.getDouble("taxaatraso"),
										resultSet.getDouble("desconto"),
										resultSet.getString("formapagamento") );
			
			pagamento.setLocacao(LocacaoDAO.getLocacao(resultSet.getLong("locacao_id")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pagamento;
	}
}
