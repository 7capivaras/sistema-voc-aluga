package view.consultas;

import java.util.ArrayList;

import control.Controle;
import model.Manutencao;
import model.Util;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Lista;
import view.manutencao.FinalizarManutencao;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaManutencoes extends Lista<Manutencao> {

	private static ListaManutencoes listaManutencoes = null;
	private ArrayList<Manutencao> manutencoes;
	
	public ListaManutencoes() {
		super("Lista de Manuten��es");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		String[] cabecalho = new String[]{	"C�digo Manuten��o",
				"Placa Carro", 
				"Data Agendada", 
				"Tipo de Servi�o"};
		
		super.setCabecalho(cabecalho);
		super.setModel();
		super.setSelecaoSigular();
		
		JButton btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDados();
			}
		});
		btnCarregar.setBounds(174, 291, 89, 23);
		add(btnCarregar);
		
		JButton btnFinalizarManuteno = new JButton("Finalizar Manuten\u00E7\u00E3o");
		btnFinalizarManuteno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				finalizarManutencao();
			}
		});
		btnFinalizarManuteno.setBounds(283, 291, 163, 23);
		add(btnFinalizarManuteno);
	}
	
	public static ListaManutencoes getInstance(){
		if(listaManutencoes == null){
			listaManutencoes = new ListaManutencoes();
		}
		
		return listaManutencoes;
	}

	private void finalizarManutencao() {
		super.trocaTela(FinalizarManutencao.getInstance());
	}

	protected void setDados() {
		this.manutencoes = Controle.getManutencoes();
		super.carregaDados(manutencoes);
	}
	
	protected String[] getTupla(Manutencao m){
		String[] tupla = new String[]{ 	m.getIdManutencao()+"", 
				m.getCarro().getPlaca()+"", 
				Util.calendarToString(m.getDataAgendada()), 
				m.getTipoServico() };
		return tupla;
	}
	
}