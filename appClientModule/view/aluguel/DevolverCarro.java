package view.aluguel;
import javax.swing.JButton;

import model.Cliente;
import model.Locacao;
import model.Util;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;

import view.Lista;
import view.Mascara;
import control.Controle;


@SuppressWarnings("serial")
public class DevolverCarro extends Lista<Locacao> {

	private static DevolverCarro devolverCarro = null;
	private JFormattedTextField jFTextFieldCpf;
	private JTextField textFieldKmRodados;
	private Cliente cliente;
	
	private DevolverCarro() {
		super("Devolu��o de Carro");
		String[] cabecalho = new String[]{	"Nome Cliente", 
											"Placa do Carro", 
											"Data Retirada", 
											"Previs�o Entrega"};
		super.setCabecalho(cabecalho);
		super.setSelecaoSigular();
		
		JButton btnRegistrarEntrega = new JButton("Registrar Entrega");
		btnRegistrarEntrega.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				registraEntrega();
			}
		});
		btnRegistrarEntrega.setBounds(303, 277, 143, 23);
		add(btnRegistrarEntrega);
		
		JButton btnBuscaLocaes = new JButton("Buscar Loca\u00E7\u00F5es");
		btnBuscaLocaes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscaLocacoes();
			}
		});
		btnBuscaLocaes.setBounds(303, 55, 143, 23);
		add(btnBuscaLocaes);
		
		jFTextFieldCpf = new JFormattedTextField(Mascara.mascaraCPF());
		jFTextFieldCpf.setBounds(199, 56, 94, 20);
		add(jFTextFieldCpf);
		
		JLabel lblCpf = new JLabel("CPF do Cliente:");
		lblCpf.setBounds(108, 59, 94, 14);
		add(lblCpf);
		
		textFieldKmRodados = new JTextField();
		textFieldKmRodados.setBounds(207, 278, 86, 20);
		add(textFieldKmRodados);
		textFieldKmRodados.setColumns(10);
		
		JLabel lblKmRodados = new JLabel("km Rodados:");
		lblKmRodados.setBounds(108, 281, 89, 14);
		add(lblKmRodados);
		
		this.cliente = null;
	}
	
	public static DevolverCarro getInstance(){
		if( devolverCarro == null){
			devolverCarro = new DevolverCarro();
		}
		
		return devolverCarro;
	}
	
	protected String[] getTupla(Locacao l){
		String[] tupla = new String[]{ 	l.getCliente().getNome(), 
										l.getCarro().getPlaca(),  
										Util.calendarToString(l.getDataRetirada()),
										Util.calendarToString(l.getPrevisaoEntrega())};
		return tupla;
	}
	
	private void buscaLocacoes(){
		super.apagaFormulario();
		long cpf = Util.StringToLong(this.jFTextFieldCpf.getText().replaceAll("[_.-]", ""));
		this.cliente = Controle.getCliente(cpf);
		if( this.cliente == null ){
			super.setMensagemErro("Cliente n�o foi encontrado!");
			return;
		}
		
		this.cliente = Controle.getDevolucoesAbertas(this.cliente);
		if( this.cliente.getLocacoes().size() == 0 ){
			super.setMensagemAlerta("Nenhuma devolu��o em aberto encontrada para esse cliente!");
			return;
		}
		super.carregaDados(this.cliente.getLocacoes());
		super.setMensagemConclusao("Os dados das loca��es foram carregados!");
	}
	
	private void registraEntrega(){
		
		Locacao locacao = super.getDadoSelecionado();
		if(locacao == null){
			super.setMensagemAlerta("Nenhuma loca��o foi selecionada!");
			return;
		}
		
		if( !this.textFieldKmRodados.getText().replaceAll("[.0-9]", "").equals("") ){
			super.setMensagemAlerta("Os Km rodados informados n�o � um n�mero!");
			return;
		}
		
		if( this.textFieldKmRodados.getText().replaceAll("[^.0-9]", "").equals("") ){
			super.setMensagemAlerta("Os Km rodados do carro n�o foi informado!");
			return;
		}

		double km = Util.stringToDouble(this.textFieldKmRodados.getText());
		locacao.getCarro().setKmRodados(km);
		locacao.setDataEntregaHoje();
		
		if( !Controle.registraDevolucao(locacao) ){
			super.setMensagemErro("Falha de conex�o ao registrar devolu��o!");
			return;
		}
		
		super.removeDadoSelecionado();
		super.setMensagemConclusao("Devolu��o registrada com sucesso!");
	}
	
	public void apagaFormulario(){
		super.apagaFormulario();
		this.textFieldKmRodados.setText("");
		this.jFTextFieldCpf.setText("");
	}
}
