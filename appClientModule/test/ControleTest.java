package test;

import java.util.ArrayList;
import java.util.Calendar;

import model.*;
import control.*;
import control.DAO.AgenciaDAO;
import junit.framework.*;

import org.junit.Test;

public class ControleTest extends TestCase{

	
	@Test
	public void testConectarSistema(){
		long cpf1 = 11111111111L;
		long cpf2 = 11111111110L;
		String senha1 = "bob";
		String senha2 = "bobo";
		
		Funcionario resultadoObtido1 = Controle.conectarSistema(cpf1, senha1);
		Funcionario resultadoObtido2 = Controle.conectarSistema(cpf1, senha2);
		Funcionario resultadoObtido3 = Controle.conectarSistema(cpf2, senha2);
		
		
		assertNotNull("O CPF e senha v�lidos, ent�o funcion�rio deve ser diferente de nulo", resultadoObtido1);
		assertNull("O CPF v�lido e senha inv�lida, ent�o funcion�rio deve ser nulo", resultadoObtido2);
		assertNull("O CPF inv�lido e senha inv�lida, ent�o funcion�rio deve ser nulo",resultadoObtido3);
	}
	
	@Test
	public void testGetFuncionarios(){
		ArrayList<Funcionario> af = Controle.getFuncionarios();
		
		assertNotNull("retorna todos os funcionarios.",af);
	}
	
	@Test
	public void testSalvarFuncionario(){
		Funcionario f = new Funcionario(	1,
											11111111111L,
											"Administrador",
											"Rua 995",
											"(21)999-999-999",
											"gerente",
											"bob");
		
		Agencia a = AgenciaDAO.getAgenciaById(1);
		f.setAgencia(a);
		
		boolean resposta = Controle.salvarFuncionario(f);
		
		assertEquals("Passado como parametro o funcionario de testes, retorna true se salvo",true,resposta);
	}
	
	@Test
	public void testGetFuncionario(){
		long cpf1 = 11111111111L;
		long cpf2 = 11111111110L;
		
		
		Funcionario f1 = Controle.getFuncionario(cpf1);
		Funcionario f2 = Controle.getFuncionario(cpf2);
		
		assertNotNull("O cpf v�lido, retorno esperado diferente de nulo.",f1);
		assertNull("O cpf inv�lido, retorno esperado nulo.",f2);
	}
	
	@Test
	public void testGetCarros(){
		ArrayList<Carro> ac = Controle.getCarros();
		
		assertNotNull("retorna todos os carros.",ac);
	}
	
	@Test
	public void testGetCarro(){
		Carro c1 = Controle.getCarro("PLC-1234");
		Carro c2 = Controle.getCarro("PLC-0000");
		
		assertNotNull("Retorna uma inst�ncia de carro.",c1);
		assertNull("Retorna nulo.",c2);
	}
	
	@Test
	public void testSalvarCarro(){
		Calendar dt = Calendar.getInstance();
		dt.set(2014, 10, 20);

		Calendar dt2 = Calendar.getInstance();
		dt2.set(2014, 10, 21);
		
		Carro c = new Carro( 	1,
								"fa89f6a98df7a9s7d",
								"Pagani",
								"Zonda R",
								"bonito",
								dt,
								50.0,
								dt2,
								"PLC-1234",
								1000.0);
		
		Agencia agencia = AgenciaDAO.getAgenciaById(1);
		c.setAgencia(agencia);
		
		boolean resposta = Controle.salvarCarro(c);
		
		assertEquals("Retorna true se o carro foi salvo com sucesso.", true, resposta);
	}
	
	@Test
	public void testGetClientes(){
		ArrayList<Cliente> ac = Controle.getClientes();
		
		assertNotNull("retorna todos os clientes.",ac);
	}
	
	@Test
	public void testGetCliente(){
		Cliente c1 = Controle.getCliente(12345678909L);
		Cliente c2 = Controle.getCliente(12345678910L);
		
		assertNotNull("Retorna uma inst�ncia de cliente.",c1);
		assertNull("Retorna nulo.",c2);
	}
	
	@Test
	public void testSalvarCliente(){

		Calendar dt = Calendar.getInstance();
		dt.set(1981, 9, 4);
		
		Cliente c = new Cliente(	1,
									12345678909L,
									"Beyonc� Giselle Knowles-Carter",
									"Copacabana Palace",
									"21999999999",
									dt);
		
		boolean resposta = Controle.salvarCliente(c);
		
		assertEquals("Retorna true se salvar os dados do cliente com sucesso", true, resposta);
	}

}

