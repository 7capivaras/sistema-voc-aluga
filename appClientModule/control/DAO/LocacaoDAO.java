package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Cliente;
import model.Locacao;
import model.Motorista;
import model.Util;

import java.util.ArrayList;
import java.util.Calendar;

import control.ConnectionFactory;


public class LocacaoDAO {

	public static boolean haLocacao(Calendar data, long idCarro) {

		boolean haLocacao = false;
		
		java.sql.Date dataInicio = Util.calendarToSQL(data);
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Locacao where carro_id = ? and "
					+ "previsaoentrega >= ? ");
			stmt.setLong(1, idCarro);
			stmt.setDate(2, dataInicio);
			ResultSet resultSet = stmt.executeQuery();
			
			if (resultSet.next()){
				haLocacao = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return haLocacao;
	}
	
	private static ArrayList<Locacao> idsLocacao;

	public static ArrayList<Locacao> buscarLocacoesAntigas(long idCliente) {
		
		idsLocacao = new ArrayList<Locacao>();
		java.sql.Date dataAtual = Util.calendarToSQL(Calendar.getInstance());
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select idlocacao from voce_aluga.Locacao where cliente_id = ? and dataentrega <= ?");
			stmt.setLong(1, idCliente);
			stmt.setDate(2, dataAtual);
			ResultSet resultSet = stmt.executeQuery();
			
			while( resultSet.next() ){
				Locacao locacao = new Locacao(resultSet.getLong("idlocacao"));
				idsLocacao.add(locacao);
			}
		
		}catch (SQLException e) {
			e.printStackTrace();
			idsLocacao = null;
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return idsLocacao;
	}	
	
	public static boolean alugarCarro ( Calendar previsaoentrega, Calendar dataretirada, long agencia_id, long cartaocredito_id,
			long cliente_id, long carro_id){
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		boolean concluido = false;
		
		java.sql.Date dataLocacaoSQL = Util.calendarToSQL(Calendar.getInstance());
		java.sql.Date previsaoEntregaSQL = Util.calendarToSQL(previsaoentrega);
		java.sql.Date dataRetiradaSQL = Util.calendarToSQL(dataretirada);
		
		try {
			stmt = con.prepareStatement("INSERT INTO voce_aluga.locacao(" +
						"datalocacao, previsaoentrega, dataretirada, " +
						"agencia_id, cartaocredito_id, cliente_id, carro_id)"+
						"VALUES (?, ?, ?, ?, ?, ?, ?);");
			
			stmt.setDate(1, dataLocacaoSQL);
			stmt.setDate(2, previsaoEntregaSQL);
			stmt.setDate(3, dataRetiradaSQL);
			stmt.setLong(4, agencia_id);
			stmt.setLong(5, cartaocredito_id);
			stmt.setLong(6, cliente_id);
			stmt.setLong(7, carro_id);

			stmt.execute();
			concluido = true;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;		
	}
	
	public ArrayList<Locacao> getLocacoes(){
		ArrayList<Locacao> locacoes = new ArrayList<Locacao>();
		Locacao locacao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Locacao");
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				locacao = getLocacao(resultSet);
				locacoes.add(locacao);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return locacoes;
	}

	public static Locacao getLocacao(long id) {
		
		Locacao locacao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select * from voce_aluga.locacao where dataentrega = "
					+ "(select MAX(dataentrega) from voce_aluga.Locacao where carro_id = ?)");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				locacao = getLocacao(resultSet);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return locacao;
	}
	
	public static Locacao getLocacaoById(long id) {
		
		Locacao locacao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select * from voce_aluga.locacao where idlocacao = ?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				locacao = getLocacao(resultSet);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return locacao;
	}
	
	public static Locacao getUltimaLocacao() {
		
		Locacao locacao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select l.* from voce_aluga.locacao as l, "
										+ "lateral(select last_value from voce_aluga.locacao_idlocacao_seq) as seq "
										+ "where l.idlocacao = seq.last_value;");
			
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				locacao = getLocacao(resultSet);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return locacao;
	}
	
	private static Locacao getLocacao(ResultSet resultSet){
		
		Locacao locacao = null;
		
		try {
			locacao = new Locacao(	resultSet.getLong("idlocacao"),
									Util.stringSQLToCalendar(resultSet.getString("datalocacao")),
									Util.stringSQLToCalendar(resultSet.getString("dataentrega")),
									Util.stringSQLToCalendar(resultSet.getString("previsaoentrega")),
									Util.stringSQLToCalendar(resultSet.getString("dataretirada")) );
			
			locacao.setAgencia(AgenciaDAO.getAgenciaById(resultSet.getLong("agencia_id")));
			locacao.setCliente(ClienteDAO.getClienteById(resultSet.getLong("cliente_id")));
			locacao.setCarro(CarroDAO.getCarroById(resultSet.getLong("carro_id")));
			locacao.setCartaoCredito(CartaoCreditoDAO.getCartaoById(resultSet.getLong("cartaocredito_id")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return locacao;
	}
	
	private static Locacao getLocacao(ResultSet resultSet, Cliente cliente){
		
		Locacao locacao = null;
		
		try {
			locacao = new Locacao(	resultSet.getLong("idlocacao"),
									Util.stringSQLToCalendar(resultSet.getString("datalocacao")),
									Util.stringSQLToCalendar(resultSet.getString("dataentrega")),
									Util.stringSQLToCalendar(resultSet.getString("previsaoentrega")),
									Util.stringSQLToCalendar(resultSet.getString("dataretirada")) );
			
			locacao.setAgencia(AgenciaDAO.getAgenciaById(resultSet.getLong("agencia_id")));
			locacao.setCliente(cliente);
			locacao.setCarro(CarroDAO.getCarroById(resultSet.getLong("carro_id")));
			locacao.setCartaoCredito(CartaoCreditoDAO.getCartaoById(resultSet.getLong("cartaocredito_id")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return locacao;
	}

	public static Cliente getDevolucoesAbertas(Cliente cliente) {
		if(cliente == null)
			return null;
		
		Locacao locacao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select * from voce_aluga.locacao "
					+ "where cliente_id = ? and dataentrega is null;");
			
			stmt.setLong(1, cliente.getId());
			
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				locacao = getLocacao(resultSet,cliente);
				cliente.addLocacao(locacao);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cliente;
	}

	public static boolean registraDevolucao(Locacao locacao) {
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		boolean concluido = false;
		
		try{
			
			stmt = con.prepareStatement("UPDATE voce_aluga.Locacao "
									  + "SET dataentrega=? "
									  + "WHERE idLocacao = ? ;");
			java.sql.Date dataEntregaSQL = Util.calendarToSQL(locacao.getDataEntrega());
			stmt.setDate(1, dataEntregaSQL);
			stmt.setLong(2, locacao.getIdLocacao());
			stmt.execute();
			
			concluido = true;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}

	public static Cliente getLocacoesAbertas(Cliente cliente) {
		if(cliente == null)
			return null;
		
		Locacao locacao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select l.* from voce_aluga.locacao as l "+
											"left outer join voce_aluga.pagamento as p "+
											"on l.idlocacao = p.locacao_id "+
											"where l.cliente_id = ? and ( l.dataentrega is null or p.locacao_id is null )"+
											"group by idLocacao;");
			stmt.setLong(1, cliente.getId());
			
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				locacao = getLocacao(resultSet,cliente);
				cliente.addLocacao(locacao);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cliente;
	}
	
	public static ArrayList<Locacao> buscaLocacoesDoMotorista(ArrayList<Motorista> motoristas){
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		Locacao locacao = null;
		ArrayList<Locacao> locacoes = new ArrayList<Locacao>();
						
		try {
			for(Motorista m: motoristas){
				stmt = con.prepareStatement("select * from voce_aluga.locacao where idlocacao = ? group by idlocacao");
				stmt.setLong(1, m.getLocacao().getIdLocacao());
				
				ResultSet resultset = stmt.executeQuery();
	
				while (resultset.next()){
					locacao = getLocacao(resultset);					
					locacoes.add(locacao);
				}
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return locacoes;
	}
}
