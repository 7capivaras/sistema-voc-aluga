package control;

import java.util.ArrayList;
import java.util.Calendar;

import control.DAO.AgenciaDAO;
import control.DAO.AutorizacaoDAO;
import control.DAO.CarroDAO;
import control.DAO.CartaoCreditoDAO;
import control.DAO.ClienteDAO;
import control.DAO.FuncionarioDAO;
import control.DAO.LocacaoDAO;
import control.DAO.ManutencaoDAO;
import control.DAO.MotoristaDAO;
import control.DAO.PagamentoDAO;
import control.DAO.ReservaDAO;
import control.DAO.VendaDAO;
import model.Agencia;
import model.Autorizacao;
import model.Carro;
import model.CartaoCredito;
import model.Cliente;
import model.Funcionario;
import model.Locacao;
import model.Manutencao;
import model.Motorista;
import model.Pagamento;
import model.Reserva;
import model.Util;


public class Controle {
	
	public static Funcionario conectarSistema(long cpf, String senha){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		return funcionarioDAO.verificaLogin(cpf, senha);
	}
	
	public static ArrayList<Funcionario> getFuncionarios(){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		return funcionarioDAO.getFuncionarios();
	}
	
	public static Funcionario getFuncionario(long cpf){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		return funcionarioDAO.getFuncionario(cpf);
	}
	
	public static boolean salvarFuncionario(Funcionario funcionario){
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		return funcionarioDAO.salvarFuncionario(funcionario);
	}

	public static ArrayList<Carro> getCarros(){
		CarroDAO carroDAO = new CarroDAO();
		return carroDAO.getCarros();
	}
	
	public static ArrayList<Carro> getCarrosVenda(){
		CarroDAO carroDAO = new CarroDAO();
		return carroDAO.getCarrosVenda();
	}
	
	public static Carro getCarro(String placa){
		return CarroDAO.getCarro(placa);
	}
	
	public static Carro getCarroByChassi(String chassi){
		return CarroDAO.getCarroByChassi(chassi);
	}
	
	public static Carro getCarroById(long id){
		return CarroDAO.getCarroById(id);
	}
	
	public static boolean salvarCarro(Carro carro){
		return CarroDAO.salvarCarro(carro);
	}
	
	public static ArrayList<Pagamento> getPendencias(){
		PagamentoDAO pendenciasDAO = new PagamentoDAO();
		return pendenciasDAO.getPendencias();
	}

	public static ArrayList<Locacao> getLocacoes(){
		LocacaoDAO locacaoDAO = new LocacaoDAO();
		return locacaoDAO.getLocacoes();
	}

	public static ArrayList<Reserva> getReservas(){
		ReservaDAO reservaDAO = new ReservaDAO();
		return reservaDAO.getReservas();
	}

	public static ArrayList<Manutencao> getManutencoes(){
		ManutencaoDAO manutencaoDAO = new ManutencaoDAO();
		return manutencaoDAO.getManutencoes();
	}
	
	public static ArrayList<Cliente> getClientes(){
		ClienteDAO clienteDAO = new ClienteDAO();
		return clienteDAO.getClientes();
	}
	
	public static Cliente getCliente(long cpf){
		return ClienteDAO.getCliente(cpf);
	}
	
	public static Cliente getClienteById(long id){
		return ClienteDAO.getClienteById(id);
	}
	
	public static boolean salvarCliente( Cliente cliente ){
		ClienteDAO clienteDAO = new ClienteDAO();
		return clienteDAO.salvarCliente(cliente);
	}
	
	public static boolean transferirCarro(String placa, String idAgencia){
		
		long id = Util.StringToLong(idAgencia);
		Agencia agencia = AgenciaDAO.getAgenciaById(id);
		Carro carro = Controle.getCarro(placa);
		if(carro == null)
			return false;
		
		carro.setAgencia(agencia);
		
		if(CarroDAO.salvarCarro(carro))
			return true;
		
		return false;
	}

	public static boolean verificarClienteCPF(String cpf) {		
						
		if (cpf.replaceAll("[_.-]", "") == null || cpf.replaceAll("[_.-]", "").equals(""))
			return false;
		
		return ClienteDAO.verificaCPF(Util.StringToLong(cpf.replaceAll("[_.-]", "")));
	}

	public static boolean verificaDisponibilidadeCarro(String placa, String dataRetirada, String validadeReserva) {
				
		if (placa == null || placa.replaceAll("[_-]", "").equals(""))
			return false;
		if (dataRetirada == null || dataRetirada.replaceAll("[_/]", "").equals("") ||
				validadeReserva == null || validadeReserva.replaceAll("[_/]", "").equals(""))
			return false;
		
		Carro carro = new Carro();	

		carro = CarroDAO.getCarro(placa);
		
		if (carro == null)
			return false;
		
		Calendar dataRetiradaDate = Util.stringToCalendar(dataRetirada);
		Calendar validadeReservaDate = Util.stringToCalendar(validadeReserva);
				
		if ( ReservaDAO.haReserva(dataRetiradaDate, validadeReservaDate, carro.getId()) 
				|| LocacaoDAO.haLocacao(dataRetiradaDate, carro.getId())
				|| ManutencaoDAO.haManutencao(dataRetiradaDate, validadeReservaDate, carro.getId()))
			return false;
		
		return true;
	}
	
	public static boolean reservarCarro(String cpf, String placa, String dataRetirada, 
										String validadeReserva, long idFuncionario){		
		
		if (placa == null || placa.replaceAll("[_-]", "").equals("") 
				|| cpf == null || cpf.replaceAll("[_.-]", "").equals("") 
				|| dataRetirada == null || dataRetirada.replaceAll("[_/]", "").equals("") 
				|| validadeReserva == null || validadeReserva.replaceAll("[_/]", "").equals(""))
			return false;
				
		long CPF = Util.StringToLong(cpf.replaceAll("[_.-]", ""));
		Calendar dataRetiradaDate = Util.stringToCalendar(dataRetirada);
		Calendar validadeReservaDate = Util.stringToCalendar(validadeReserva);
		
		if( validadeReservaDate == null || validadeReservaDate.before(Calendar.getInstance()))
			return false;
		
		Carro carro = CarroDAO.getCarro(placa);
		
		Cliente cliente = ClienteDAO.getCliente(CPF);
		
		if (cliente == null || carro == null)
			return false;
		
//		if (PagamentoDAO.isListaNegra(cliente.getId()))
//			return false;
		
		if (ReservaDAO.haReserva(dataRetiradaDate, validadeReservaDate, carro.getId()) 
				|| LocacaoDAO.haLocacao(dataRetiradaDate, carro.getId())
				|| ManutencaoDAO.haManutencao(dataRetiradaDate, validadeReservaDate, carro.getId()))
			return false;

		return ReservaDAO.reservarCarro(CPF, carro.getId(), cliente.getId(), dataRetiradaDate, validadeReservaDate, idFuncionario);
	}

	public static boolean buscaReserva(String cpf) {
		
		if (cpf == null || cpf.replaceAll("[_.-]", "").equals(""))
			return false;
		
		return ReservaDAO.haReserva(cpf.replaceAll("[_.-]", ""));
	}

	public static boolean venderCarro(String cpf, String placa, String valor) {
		
		if (cpf == null || placa.replaceAll("[_-]", "").equals("") || valor.equals("0"))
			return false;
		
		long CPF = Util.StringToLong(cpf.replaceAll("[_.-]", ""));
		Carro carro = CarroDAO.getCarro(placa);
		long preco = Util.StringToLong(valor);

		Cliente cliente = ClienteDAO.getCliente(CPF);
		
		if(VendaDAO.venderCarro(carro.getId(), cliente.getId(), preco))
			return true;
		else{
			return false;
		}
		
	}
		
	public static boolean alugarCarro(String cpf, String placa, String previsaoEntrega, String dataRetirada, 
										String numeroCartao, String nome, String validade, String crc, long idreserva){
		
		if (placa == null || placa.replaceAll("[_-]", "").equals("") || cpf == null || cpf.replaceAll("[_.-]", "").equals("")
				|| previsaoEntrega == null || previsaoEntrega.replaceAll("[_/]", "").equals("")
				|| dataRetirada == null || dataRetirada.replaceAll("[_/]", "").equals("") )
			return false;
			
		long CPF = Util.StringToLong(cpf.replaceAll("[_.-]", ""));
		long numero = Util.StringToLong(numeroCartao);
		
		Carro carro = CarroDAO.getCarro(placa);
		Cliente cliente = ClienteDAO.getCliente(CPF);
		
//		long idCliente	= cliente.getId();
		
//		if (PagamentoDAO.isListaNegra(idCliente))
//			return false;
		
		Calendar previsaoEntregaCal = Util.stringToCalendar(previsaoEntrega);
		Calendar dataRetiradaCal = Util.stringToCalendar(dataRetirada);
		
		if(idreserva != 0){
			ReservaDAO.removeReserva(idreserva);
		}
		
		if (ReservaDAO.haReserva(dataRetiradaCal, previsaoEntregaCal, carro.getId()) 
				|| LocacaoDAO.haLocacao(dataRetiradaCal, carro.getId())
				|| ManutencaoDAO.haManutencao(dataRetiradaCal, previsaoEntregaCal, carro.getId()))
			return false;

		if (carro == null || cliente == null)
			return false;
		
		if (!cadastrarCartao(numeroCartao, nome, validade, crc))
			return false;
		

		CartaoCredito cartao = CartaoCreditoDAO.getCartao(numero);
		
		if( LocacaoDAO.alugarCarro( previsaoEntregaCal, dataRetiradaCal, carro.getAgencia().getIdAgencia(), cartao.getId(), cliente.getId(), carro.getId()) )
			return true;
		else{
			CartaoCreditoDAO.removeCartaoCreditoById(cartao.getId());
			return false;
		}
	}
	
	private static boolean cadastrarCartao(String numeroCartao, String nome,
			String validade, String crc) {

		if (numeroCartao == null || numeroCartao.isEmpty() || nome == null || nome.isEmpty() || crc == null || crc.replaceAll("[-]", "").isEmpty()
				|| validade == null || validade.replaceAll("[_/]", "").isEmpty())
			return false;
		
		validade  = "01/".concat(validade);
		
		Calendar validadeCartao = Util.stringToCalendar(validade);
		
		if( validadeCartao == null || validadeCartao.before(Calendar.getInstance()))
			return false;
		
		long numero = Util.StringToLong(numeroCartao);
		int codigoSeguranca = Integer.parseInt(crc);
		
		CartaoCredito cartao = new CartaoCredito(	0,
													numero,
													codigoSeguranca,
													nome,
													validadeCartao );
		
		return CartaoCreditoDAO.cadastrarCartao(cartao);
	}

	public static boolean agendarManutencao(String placa, String data, String tipoServico, String motivo, long idFuncionario){
		
		if (placa == null || placa.replaceAll("[_-]", "").equals("") || data == null || data.replaceAll("[_/]", "").equals("") 
				|| tipoServico == null|| tipoServico.isEmpty() || motivo == null || motivo.isEmpty() )
			return false;
		
		Calendar dataManutencao = Util.stringToCalendar(data);
		
		if( dataManutencao == null || dataManutencao.before(Calendar.getInstance()))
			return false;
		
		Carro carro = CarroDAO.getCarro(placa);
		
		if (carro == null)
			return false;
		
		if (ReservaDAO.haReserva(dataManutencao,dataManutencao,carro.getId()) || LocacaoDAO.haLocacao(dataManutencao, carro.getId())
				|| ManutencaoDAO.haManutencao(dataManutencao, dataManutencao, carro.getId()))
			return false;

		return ManutencaoDAO.agendarManutencao(carro.getId(), dataManutencao, tipoServico, motivo, idFuncionario);
	}

	public static boolean finalizarManutencao(String placa) {
		
		if (placa == null || placa.replaceAll("[_-]", "").isEmpty())
			return false;
		
		Carro carro = CarroDAO.getCarro(placa);
		
		if (carro == null)
			return false;
		
		long idManutencao = ManutencaoDAO.idManutencaoEmAndamento(carro.getId());
		
		if (idManutencao > 0)
			return ManutencaoDAO.finalizarManutencao(idManutencao);
		
		return false;
	}

	public static Locacao getLocacao(String placa) {

		if (placa == null || placa.replaceAll("[_-]", "").isEmpty())
		return null;
	
		Carro carro = CarroDAO.getCarro(placa);
		
		if (carro == null)
			return null;
		
		return LocacaoDAO.getLocacao(carro.getId());
	}

	public static Cliente getDevolucoesAbertas(Cliente cliente) {
		return LocacaoDAO.getDevolucoesAbertas(cliente);
	}

	public static boolean registraDevolucao(Locacao locacao) {
		CarroDAO.salvarCarro(locacao.getCarro());
		return LocacaoDAO.registraDevolucao(locacao);
	}

	public static Cliente getPendencias(Cliente cliente) {
		return LocacaoDAO.getLocacoesAbertas(cliente);
	}

	public static boolean registraPagamentoLocacao(Pagamento p) {
		return PagamentoDAO.registrarPagamento(p);
	}

	public static Locacao getPagamentosLocacao(Locacao locacao) {
		return PagamentoDAO.getPagamentosLocacao(locacao);
	}
	
	public static boolean registraMotoristas(Locacao l){
		return MotoristaDAO.salvarMotoristas(l);
	}
	
	public static Locacao getUltimaLocacao(){
		return LocacaoDAO.getUltimaLocacao();
	}

	public static ArrayList<Reserva> getReservasAbertas() {
		return ReservaDAO.getReservasAbertas();
	}

	public static ArrayList<Locacao> buscarLocacoes(String text) {
		ArrayList<Motorista> registros = MotoristaDAO.buscarRegistros(text);
		ArrayList<Locacao> locacoes = LocacaoDAO.buscaLocacoesDoMotorista(registros);
		return locacoes;
	}

	public static boolean registraRetirada(Autorizacao autorizacao) {
		return AutorizacaoDAO.registraRetirada(autorizacao);
	}

	public static Motorista getMotoristaByCpf(long cpf) {
		Motorista motorista = MotoristaDAO.getMotoristaById(cpf);
		return motorista;
	}
}
