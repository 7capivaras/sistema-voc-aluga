package model;


public class Funcionario
{
	private long id;
	private long CPF;
	private String nome;
	private String endereco;
	private String telefone;
	private String cargo;
	private String senhaSistema;
	
	private Agencia agencia;
	
	public static final String CARGO_GERENTE = "gerente";
	public static final String CARGO_AGENTE = "agente";

	public Funcionario(	long id,
						long cpf,
						String nome,
						String endereco,
						String telefone,
						String cargo,
						String senha )
	{
		this.id = id;
		this.CPF = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
		this.cargo = cargo;
		this.senhaSistema = senha;
		
		this.agencia = null;
	}
	
	public boolean verificaSenha(String senha){
		return this.senhaSistema.equals(senha);
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public long getId() {
		return id;
	}

	public long getCPF() {
		return CPF;
	}

	public String getNome() {
		return nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getCargo() {
		return cargo;
	}

	public String getSenhaSistema() {
		return senhaSistema;
	}

	public Agencia getAgencia() {
		return agencia;
	}
	
}

