package view;

import javax.swing.JTable;

import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public abstract class Lista<T> extends Funcionalidade{
	
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel modelo;
	private String[] cabecalho;
	private ArrayList<T> listaT;
	private JComboBox<String> comboBox;
	private JLabel lblFiltrarPor;
	
	public Lista(String nome){
		super(nome);
		this.cabecalho = null;
		this.listaT = null;
		
		table = new JTable();
		this.setModel();
		
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(36, 83, 410, 167);
		add(scrollPane);
		
		lblFiltrarPor = new JLabel("");
		lblFiltrarPor.setFont(new Font("Arial", Font.BOLD, 12));
		lblFiltrarPor.setBounds(219, 54, 66, 14);
		add(lblFiltrarPor);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(287, 52, 159, 20);
		comboBox.setVisible(false);
		add(comboBox);
	}
	
	protected void setTamanhoLista(int altura){
		this.scrollPane.setSize(this.scrollPane.getWidth(), altura);
	}
	
	protected void setModelFiltros(String[] filtros){
		this.lblFiltrarPor.setText("Filtrar por:");
		comboBox.setVisible(true);
		comboBox.setModel(new DefaultComboBoxModel<String>(filtros));
	}

	protected void setModel(){
		String[] cabecalho = new String[]{};
		Object[][] data = new Object[][]{};
		modelo = new DefaultTableModel(data,cabecalho) {
				public Class<String> getColumnClass(int columnIndex) {
					return String.class;
				}
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			
		this.modelo.setColumnIdentifiers(this.cabecalho);
	}
	
	protected void setCabecalho(String[] cabecalho){
		this.cabecalho = cabecalho;
	}
	
	public void apagaFormulario(){
		this.setModel();
		this.table.setModel(this.modelo);
		super.limpaMensagem();
	}
	
	protected void carregaDados(ArrayList<T> listaT){
		
		setModel();
		this.listaT = listaT;
		
		if( this.listaT == null){
			this.table.setModel(this.modelo);
			return;
		}
		
		for(int i=0; i<this.listaT.size(); i++){
			this.modelo.insertRow(i, getTupla(this.listaT.get(i)) );
		}
		this.table.setModel(this.modelo);
	}
	
	protected void trocaTela(Funcionalidade f){
		int indice = this.table.getSelectedRow();
		if(indice > -1){
			Principal.getInstance().trocaPanel(f);
			f.carregaFormulario(this.listaT.get(indice));
			return;
		}
		super.setMensagemAlerta("Nenhum item foi selecionado!");
	}
	
	protected void setSelecaoSigular(){
		this.table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	protected void setSelecaoMultipla(){
		this.table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}
	
	protected T getDadoSelecionado(){
		int indice = this.table.getSelectedRow();
		if(indice > -1){
			return this.listaT.get(indice);
		}
		else
			return null;
	}
	
	protected ArrayList<T> getDadosSelecionados(){
		int[] indices = this.table.getSelectedRows();
		if(indices.length == 0){
			return null;
		}
		
		ArrayList<T> dados = new ArrayList<T>();
		for(int indice: indices){
			dados.add(this.listaT.get(indice));
		}
		
		return dados;
	}
	
	protected void removeDadoSelecionado(){
		int indice = this.table.getSelectedRow();
		if(indice > -1){
			this.listaT.remove(indice);
			this.carregaDados(this.listaT);
		}
	}
	
	protected void adicionaDado(T t){
		if( t == null)
			return;
		
		if(this.listaT == null)
			this.listaT = new ArrayList<T>();
		
		int proximo = this.modelo.getRowCount();
		this.modelo.insertRow(proximo, getTupla(t) );
		this.listaT.add(t);
		this.table.setModel(this.modelo);
	}
	
	public ArrayList<T> getListaT() {
		return listaT;
	}

	protected abstract String[] getTupla(T t);
}
