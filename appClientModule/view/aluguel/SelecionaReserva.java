package view.aluguel;

import java.util.ArrayList;

import control.Controle;
import model.Reserva;
import model.Util;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Lista;
import view.Principal;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class SelecionaReserva extends Lista<Reserva> {

	private static SelecionaReserva selecionaReserva = null;
	private ArrayList<Reserva> reservas;
	private Funcionalidade jplOrigem;
	
	private SelecionaReserva() {
		super("Lista de Reservas");
		String[] cabecalho = new String[]{	"C�digo Reserva",
											"Nome Cliente", 
											"Placa Carro", 
											"Data Retirada" };
		super.setCabecalho(cabecalho);
		super.setModel();
		
		JButton btnSelecionar = new JButton("Selecionar reserva");
		btnSelecionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selecionarReserva();
			}
		});
		btnSelecionar.setBounds(295, 291, 151, 23);
		add(btnSelecionar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelarSelecao();
			}
		});
		btnCancelar.setBounds(167, 291, 89, 23);
		add(btnCancelar);
	}
	
	public static SelecionaReserva getInstance(){
		if(selecionaReserva == null){
			selecionaReserva = new SelecionaReserva();
		}
		
		return selecionaReserva;
	}
	
	private void setDados() {
		this.reservas = Controle.getReservasAbertas();
		super.carregaDados(reservas);
	}
	
	protected String[] getTupla(Reserva r){
		String[] tupla = new String[]{ 	r.getIdReserva()+"", 
										r.getCliente().getNome(), 
										r.getCarro().getPlaca(), 
										Util.calendarToString(r.getDataRetirada()) };
		return tupla;
	}
	
	private void selecionarReserva() {
		super.trocaTela(this.jplOrigem);
	}
	
	private void cancelarSelecao() {
		Principal.getInstance().trocaPanel(this.jplOrigem);
	}
	
	public void escolheReserva(Funcionalidade f){
		this.jplOrigem = f;
		Principal.getInstance().trocaPanel(this);
		this.setDados();
	}
}
