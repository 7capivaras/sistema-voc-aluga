package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {
			
	public static java.sql.Date calendarToSQL(Calendar calendar){
		
		java.sql.Date dataSQL;
		try {
			dataSQL = new java.sql.Date(calendar.getTimeInMillis());
		} catch (Exception e) {
			return null;
		}
		
		return dataSQL;
	}
	
	public static java.sql.Date dateToSQL(Date date){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		java.sql.Date dataSQL;
		try {
			dataSQL = new java.sql.Date(calendar.getTimeInMillis());
		} catch (Exception e) {
			return null;
		}
		
		return dataSQL;
	}
	
	public static String dateToString(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String strmes;
		if( calendar.get(Calendar.MONTH) < 9 ){
			strmes = "0"+(calendar.get(Calendar.MONTH)+1);
		}
		else{
			strmes = ""+(calendar.get(Calendar.MONTH)+1);
		}
		return 	calendar.get(Calendar.DAY_OF_MONTH)+"/"+
				strmes+"/"+
				calendar.get(Calendar.YEAR);
	}
	
	public static String calendarToString(Calendar calendar){
		if( calendar != null){
			String strmes;
			String strdia;
			if( calendar.get(Calendar.MONTH) < 9 ){
				strmes = "0"+(calendar.get(Calendar.MONTH)+1);
			}
			else{
				strmes = ""+(calendar.get(Calendar.MONTH)+1);
			}
			
			if( calendar.get(Calendar.DAY_OF_MONTH) < 10 ){
				strdia = "0"+calendar.get(Calendar.DAY_OF_MONTH);
			}
			else{
				strdia = ""+calendar.get(Calendar.DAY_OF_MONTH);
			}
			
			return 	strdia+"/"+
					strmes+"/"+
					calendar.get(Calendar.YEAR);
		}
		return "";
	}
	
	public static Calendar stringToCalendar( String date ){
		
		if(date != null){
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			formatter.setLenient(false);
			
			Calendar calendar = Calendar.getInstance();
			try {
				Date dt = formatter.parse(date);
				calendar.setTime(dt);			
			} catch (ParseException e) {
				return null;
			}
			return calendar;
		}
		return null;
	}
	
	public static Calendar stringSQLToCalendar( String date ){
		if( date != null ){
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();
			try {
				Date dt = formatter.parse(date);
				calendar.setTime(dt);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return calendar;
		}
		return null;
	}
	
	public static double stringToDouble(String doublestr){
		double d = 0.0;
		try{
			d = Double.parseDouble(doublestr);
		}
		catch(NumberFormatException e){
			e.printStackTrace();
		}
		return d;
	}
	
	public static long StringToLong(String longStr){
		
		long l = 0;
		
		try {
			l  = Long.parseLong(longStr);
		}
		catch (NumberFormatException e){
			e.printStackTrace();
		}
		
		return l;
	}
	
	public static boolean validarCPF(long cpf){

		long somadigitos = 0;
		long digito = 0;
		long digitosVerificadores = cpf%100;
		for(int i = 2; i <= 10; i++){
			digito = (long)(cpf/Math.pow(10, i)) - ((long)(cpf/Math.pow(10, i+1)))*10;
			somadigitos += i * digito;
		}
		
		if( somadigitos%11 < 2 ){
			if( digitosVerificadores/10 != 0 ){
				return false;
			}
		}else{
			if( (11-somadigitos%11) != digitosVerificadores/10 ){
				return false;
			}
		}

		digito = 0;
		somadigitos = 0;
		
		for(int i = 2; i <= 11; i++){
			digito = (long)(cpf/Math.pow(10, i-1)) - ((long)(cpf/Math.pow(10, i)))*10;
			somadigitos += i * digito;
		}
		
		if( somadigitos%11 < 2 ){
			if( digitosVerificadores%10 != 0 )
				return false;
		}else{
			if( (11-somadigitos%11) != digitosVerificadores%10 )
				return false;
		}

		return true;
	}

}
