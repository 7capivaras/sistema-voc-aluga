package model;
import java.util.Calendar;


public class CartaoCredito
{
	private long id;
	private long numero;
	private int crc;
	private String nome;
	private Calendar validade;

	public CartaoCredito(){
		super();
	}
	
	public CartaoCredito(	long id,
							long numero,
							int crc,
							String nome,
							Calendar validade )
	{
		this.id = id;
		this.numero = numero;
		this.crc = crc;
		this.nome = nome;
		this.validade = validade;
	}

	public long getId() {
		return id;
	}

	public long getNumero() {
		return numero;
	}

	public int getCrc() {
		return crc;
	}

	public String getNome() {
		return nome;
	}

	public Calendar getValidade() {
		return validade;
	}
}

