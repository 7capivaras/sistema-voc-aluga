package view.aluguel;

import java.util.ArrayList;

import control.Controle;
import model.Carro;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Lista;
import view.Principal;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class SelecionaCarro extends Lista<Carro> {

	private static SelecionaCarro selecionaCarro = null;
	private ArrayList<Carro> carros;
	private Funcionalidade jplOrigem;
	
	private SelecionaCarro() {
		super("Lista de Carros");
		String[] cabecalho = new String[]{"Marca","Modelo","Placa","Valor da di�ria"};
		super.setCabecalho(cabecalho);
		super.setModel();
		
		JButton btnSelecionar = new JButton("Selecionar carro");
		btnSelecionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selecionarCarro();
			}
		});
		btnSelecionar.setBounds(315, 291, 131, 23);
		add(btnSelecionar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelarSelecao();
			}
		});
		btnCancelar.setBounds(167, 291, 89, 23);
		add(btnCancelar);
	}
	
	public static SelecionaCarro getInstance(){
		if(selecionaCarro == null){
			selecionaCarro = new SelecionaCarro();
		}
		
		return selecionaCarro;
	}

	private void setDados(){
		this.carros = Controle.getCarros();
		super.carregaDados(carros);
	}
	
	private void setDadosVenda(){
		this.carros = Controle.getCarrosVenda();
		super.carregaDados(carros);
	}
	
	private void selecionarCarro(){
		super.trocaTela(this.jplOrigem);
	}

	private void cancelarSelecao() {
		Principal.getInstance().trocaPanel(this.jplOrigem);
	}
	
	public void escolheCarro(Funcionalidade f){
		this.jplOrigem = f;
		Principal.getInstance().trocaPanel(this);
		this.setDados();
	}

	public void escolheCarroVenda(Funcionalidade f){
		this.jplOrigem = f;
		Principal.getInstance().trocaPanel(this);
		this.setDadosVenda();
	}
	
	protected String[] getTupla(Carro c){
		String[] tupla = new String[]{ c.getMarca(), c.getModelo(), c.getPlaca(), ""+c.getValorDiaria() };
		return tupla;
	}
}
