package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import control.ConnectionFactory;
import model.CartaoCredito;
import model.Util;

public class CartaoCreditoDAO {

	public static boolean cadastrarCartao(CartaoCredito cartao) {
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
				
		try {
			stmt = con.prepareStatement("insert into voce_aluga.cartaocredito"
					+ "(crc, nome, numero, validade)"
					+ " values (?, ?, ?, ?)");
			

			java.sql.Date validadeCartaoSQL = Util.calendarToSQL(cartao.getValidade());
			
			stmt.setInt(1, cartao.getCrc());
			stmt.setString(2, cartao.getNome());
			stmt.setLong(3, cartao.getNumero());
			stmt.setDate(4, validadeCartaoSQL);

			stmt.execute();
			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			
			throw new RuntimeException(e);
		}
		
		return true;
	}

	public static CartaoCredito getCartao(long numero) {
		
		CartaoCredito cartao = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.CartaoCredito where numero = ?");
			stmt.setLong(1, numero);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				cartao = getCartao(resultSet);
			}
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
			cartao = null;
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cartao;
	}
	
	public static void removeCartaoCreditoById(long id){
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("delete from voce_aluga.CartaoCredito where id = ?");
			stmt.setLong(1, id);
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static CartaoCredito getCartaoById(long id) {
		
		CartaoCredito cartao = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.CartaoCredito where id = ?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				cartao = getCartao(resultSet);
			}
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
			cartao = null;
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cartao;
	}
	
	private static CartaoCredito getCartao(ResultSet resultSet){
		
		CartaoCredito cartao = null;
		
		try{
			cartao = new CartaoCredito(	resultSet.getLong("id"),
					resultSet.getLong("numero"),
					resultSet.getInt("crc"),
					resultSet.getString("nome"),
					Util.stringSQLToCalendar(resultSet.getString("validade")) );
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cartao;
	}

}
