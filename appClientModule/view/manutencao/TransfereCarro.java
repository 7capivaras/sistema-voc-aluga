package view.manutencao;

import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import view.Funcionalidade;
import view.Mascara;
import control.Controle;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class TransfereCarro extends Funcionalidade {
	
	private static TransfereCarro transfereCarro = null;
	
	private JTextField textFieldId;
	private JFormattedTextField jFTextFieldPlaca;
	
	private TransfereCarro() {
		super("Transfere Carro");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		textFieldId = new JTextField();
		textFieldId.setBounds(204, 151, 86, 20);
		add(textFieldId);
		textFieldId.setColumns(10);
		
		jFTextFieldPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		jFTextFieldPlaca.setBounds(204, 182, 86, 20);
		add(jFTextFieldPlaca);
		
		JButton btnSelecionarCarro = new JButton("Transferir carro");
		btnSelecionarCarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				transferirCarro();
			}
		});
		btnSelecionarCarro.setBounds(300, 181, 139, 23);
		add(btnSelecionarCarro);
		
		JLabel lblPlacaDoCarro = new JLabel("Placa do Carro");
		lblPlacaDoCarro.setBounds(39, 185, 147, 14);
		add(lblPlacaDoCarro);
		
		JLabel lblIdDaAgencia = new JLabel("Id da Ag\u00EAncia de Destino");
		lblIdDaAgencia.setBounds(39, 154, 147, 14);
		add(lblIdDaAgencia);
	}
	
	public static TransfereCarro getInstance(){
		if(transfereCarro == null){
			transfereCarro = new TransfereCarro();
		}
		return transfereCarro;
	}

	private void transferirCarro() {
		if( this.textFieldId.getText() != null 
				&& !this.jFTextFieldPlaca.getText().replaceAll("[_-]", "").isEmpty() 
				&& this.textFieldId.getText().matches("[0-9]*") ){
			if(Controle.transferirCarro(this.jFTextFieldPlaca.getText(), textFieldId.getText())){
				super.setMensagemConclusao("Transfererido para a adgencia: " + textFieldId.getText());
				return;
			}
			super.setMensagemErro("Erro na transferência!");
		}
		else {
			super.setMensagemAlerta("Campos preenchidos incorretamente!");
		}
	}
	
	public void apagaFormulario(){
		this.textFieldId.setText("");
		this.jFTextFieldPlaca.setText("");
		super.limpaMensagem();
	}
}
