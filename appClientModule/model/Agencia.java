package model;
import java.util.ArrayList;

public class Agencia
{
	private long idAgencia;
	private String endereco;
	private int capacidadeCarros;
	private String nome;
	
	private ArrayList<Carro> carros;
	private ArrayList<Funcionario> funcionarios;
	private ArrayList<Locacao> locacoes;
	
	public Agencia( long idAgencia,
					String nome,
					String endereco,
					int capacidadeCarros)
	{
		this.idAgencia = idAgencia;
		this.nome = nome;
		this.endereco = endereco;
		this.capacidadeCarros = capacidadeCarros;
		
		this.carros = new ArrayList<Carro>();
		this.funcionarios = new ArrayList<Funcionario>();
		this.locacoes = new ArrayList<Locacao>();
	}
	
	public long getIdAgencia() {
		return idAgencia;
	}

	public int getCapacidadeCarros() {
		return capacidadeCarros;
	}

	public String getEndereco() {
		return this.endereco;	
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void addCarro( Carro c ){
		this.carros.add(c);
	}
	
	public void addFuncionario( Funcionario f ){
		this.funcionarios.add(f);
	}
	
	public void addLocacao( Locacao l ){
		this.locacoes.add(l);
	}
	
}

