package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import view.aluguel.AlugarCarro;
import view.aluguel.DevolverCarro;
import view.aluguel.ReservaCarro;
import view.aluguel.RetirarCarro;
import view.cadastros.MantemCarro;
import view.cadastros.MantemCliente;
import view.cadastros.MantemFuncionario;
import view.consultas.ListaCarros;
import view.consultas.ListaClientes;
import view.consultas.ListaFuncionarios;
import view.consultas.ListaLocacoes;
import view.consultas.ListaManutencoes;
import view.consultas.ListaReservas;
import view.manutencao.AgendarManutencao;
import view.manutencao.FinalizarManutencao;
import view.manutencao.TransfereCarro;
import view.manutencao.VenderCarro;
import view.pagamento.PagamentoLocacao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class Principal extends JFrame {

	private static Principal principal = null;
	private JPanel contentPane;
	
	private Login login;
	
	private JMenuBar menuBar;
	
	private MantemCarro mantemCarro;
	private MantemCliente mantemCliente;
	private MantemFuncionario mantemFuncionario;
	
	private ReservaCarro reservaCarro;
	private AlugarCarro alugarCarro;
	private RetirarCarro retirarcarro;
	private AgendarManutencao agendarManutencao;
	
	private ListaCarros listaCarros;
	private ListaClientes listaClientes;
	private ListaFuncionarios listaFuncionarios;
	private ListaLocacoes listaLocacoes;
	private ListaReservas listaReservas;
	private ListaManutencoes listaManutencoes;
	
	private FinalizarManutencao finalizarManutencao;
	
	private DevolverCarro devolverCarro;
	
	private PagamentoLocacao pagamento;
	
	private TransfereCarro transfereCarro;
	
	private VenderCarro venderCarro;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = Principal.getInstance();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private Principal() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 400);
		setTitle("Voc� Aluga");
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		this.initPanels();
		this.initMenuBar();
	}
	
	public static Principal getInstance(){
		if(principal == null){
			principal = new Principal();
		}
		return principal;
	}
	
	public void initPrincipal(){
		this.menuBar.setVisible(true);
		this.trocaPanel(this.reservaCarro);
	}
	
	private void initPanels(){
		
		login = Login.getInstance();
		getContentPane().add(login);
		
		reservaCarro = ReservaCarro.getInstance();
		
		mantemCarro = MantemCarro.getInstance();
		mantemCliente = MantemCliente.getInstance();
		mantemFuncionario = MantemFuncionario.getInstance();
		
		alugarCarro = AlugarCarro.getInstance();
		agendarManutencao = AgendarManutencao.getInstance();
		retirarcarro = RetirarCarro.getInstance();
		
		listaCarros = ListaCarros.getInstance();
		listaClientes = ListaClientes.getInstance();
		listaFuncionarios = ListaFuncionarios.getInstance();
		listaLocacoes = ListaLocacoes.getInstance();
		listaReservas = ListaReservas.getInstance();
		listaManutencoes = ListaManutencoes.getInstance();

		finalizarManutencao = FinalizarManutencao.getInstance();
		
		transfereCarro = TransfereCarro.getInstance();
		devolverCarro = DevolverCarro.getInstance();
		pagamento = PagamentoLocacao.getInstance();
		venderCarro = VenderCarro.getInstance();
		

	}
	
	private void initMenuBar(){
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.setVisible(false);
		
		JMenu mnAluguel = new JMenu("Aluguel");
		menuBar.add(mnAluguel);
		
		JMenuItem mntmAlugarCarro = new JMenuItem("Alugar Carro");
		mntmAlugarCarro.addActionListener(new MenuAction(this.alugarCarro));
		mnAluguel.add(mntmAlugarCarro);
		
		JMenuItem mntmRetirarCarro = new JMenuItem("Retirar Carro");
		mntmRetirarCarro.addActionListener(new MenuAction(this.retirarcarro));
		mnAluguel.add(mntmRetirarCarro);
		
		JMenuItem mntmReservarCarro = new JMenuItem("Reservar Carro");
		mntmReservarCarro.addActionListener(new MenuAction(this.reservaCarro));
		mnAluguel.add(mntmReservarCarro);
		
		JMenuItem mntmDevolverCarroAluguel = new JMenuItem("Devolver Carro");
		mntmDevolverCarroAluguel.addActionListener(new MenuAction(this.devolverCarro));
		mnAluguel.add(mntmDevolverCarroAluguel);
		
		JMenuItem mntmComprarCarro = new JMenuItem("Comprar Carro");
		mnAluguel.add(mntmComprarCarro);
		
		JMenuItem mntmPagamentoLocacao = new JMenuItem("Pagamento Loca\u00E7\u00E3o");
		mntmPagamentoLocacao.addActionListener(new MenuAction(this.pagamento));
		mnAluguel.add(mntmPagamentoLocacao);
		
		JMenu mnListar = new JMenu("Listar");
		menuBar.add(mnListar);
		
		JMenuItem mntmListarCarros = new JMenuItem("Carros");
		mntmListarCarros.addActionListener(new MenuAction(this.listaCarros));
		mnListar.add(mntmListarCarros);
		
		JMenuItem mntmListarClientes = new JMenuItem("Clientes");
		mntmListarClientes.addActionListener(new MenuAction(this.listaClientes));
		mnListar.add(mntmListarClientes);

		JMenuItem mntmListarFuncionarios = new JMenuItem("Funcion\u00E1rios");
		mntmListarFuncionarios.addActionListener(new MenuAction(this.listaFuncionarios));
		mnListar.add(mntmListarFuncionarios);
		
		JMenuItem mntmListarLocacoes = new JMenuItem("Locacoes");
		mntmListarLocacoes.addActionListener(new MenuAction(this.listaLocacoes));
		mnListar.add(mntmListarLocacoes);
		
		JMenuItem mntmListarReservas = new JMenuItem("Reservas");
		mntmListarReservas.addActionListener(new MenuAction(this.listaReservas));
		mnListar.add(mntmListarReservas);

		JMenuItem mntmListarManutencao = new JMenuItem("Manutencao");
		mntmListarManutencao.addActionListener(new MenuAction(this.listaManutencoes));
		mnListar.add(mntmListarManutencao);
		
		JMenu mnCadastrar = new JMenu("Cadastrar");
		menuBar.add(mnCadastrar);
		
		JMenuItem mntmCadastrarClientes = new JMenuItem("Clientes");
		mntmCadastrarClientes.addActionListener(new MenuAction(this.mantemCliente));
		mnCadastrar.add(mntmCadastrarClientes);
		
		JMenuItem mntmCadastrarCarros = new JMenuItem("Carros");
		mntmCadastrarCarros.addActionListener(new MenuAction(this.mantemCarro));
		mnCadastrar.add(mntmCadastrarCarros);
		
		JMenuItem mntmCadastrarFuncionrios = new JMenuItem("Funcion\u00E1rios");
		mntmCadastrarFuncionrios.addActionListener(new MenuAction(this.mantemFuncionario));
		mnCadastrar.add(mntmCadastrarFuncionrios);
		
		JMenu mnGerencia = new JMenu("Manuten\u00E7\u00E3o");
		menuBar.add(mnGerencia);
		
		JMenuItem mntmAgendarManuteo = new JMenuItem("Agendar Manuten\u00E7\u00E3o");
		mntmAgendarManuteo.addActionListener(new MenuAction(this.agendarManutencao));
		mnGerencia.add(mntmAgendarManuteo);
		
		JMenuItem mntmDevolverCarroManutencao = new JMenuItem("Finalizar Manuten\u00E7\u00E3o");
		mntmDevolverCarroManutencao.addActionListener(new MenuAction(this.finalizarManutencao));
		mnGerencia.add(mntmDevolverCarroManutencao);

		JMenuItem mntmTransferirCarro = new JMenuItem("Transferir Carro");
		mntmTransferirCarro.addActionListener(new MenuAction(this.transfereCarro));
		mnGerencia.add(mntmTransferirCarro);
		
		JMenuItem mntmVenderCarro = new JMenuItem("Vender Carro");
		mntmVenderCarro.addActionListener(new MenuAction(this.venderCarro));
		mnGerencia.add(mntmVenderCarro);
	}
	
	private class MenuAction implements ActionListener{
		private Funcionalidade panel;
		
		private MenuAction(Funcionalidade jpl){
			this.panel = jpl;
		}
		
		public void actionPerformed(ActionEvent e){
			if(this.panel.verificaPermissao(Login.getInstance().getFuncionario()))
				trocaPanel(this.panel);
			else
				mensagemDeAlerta("Acesso negado!");
			
		}
	}
	
	public void trocaPanel(Funcionalidade jpl){
		this.getContentPane().removeAll();
		this.getContentPane().add(jpl, BorderLayout.CENTER);
		jpl.apagaFormulario();
		this.getContentPane().doLayout();
		this.update(getGraphics());
		this.getContentPane().revalidate();
	}
	
	public void mensagemDeAlerta(String mensagem) {
		JOptionPane.showMessageDialog(contentPane, mensagem);
	}
}
