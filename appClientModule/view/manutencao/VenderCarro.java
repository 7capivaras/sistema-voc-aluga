package view.manutencao;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;

import view.Funcionalidade;
import view.Login;
import view.Mascara;
import view.Principal;
import view.aluguel.SelecionaCarro;
import view.pagamento.ExibirNota;
import model.Carro;
import model.Cliente;
import model.Util;
import model.Venda;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import control.Controle;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Color;
import java.util.Calendar;
import java.util.Random;

import javax.swing.JTextField;


@SuppressWarnings("serial")
public class VenderCarro extends Funcionalidade{
	private static VenderCarro vendercarro = null;
	
	private JFormattedTextField jFTextCPF;
	private JFormattedTextField jFTextPlaca;
	private Random random = new Random();
	private JTextField textValorCarro;
	
	private Carro carro;
	private Cliente cliente;
		
	private VenderCarro() {
		super("Venda de Carros");
		
		JLabel label = new JLabel("Placa do carro");
		label.setBounds(10, 101, 107, 14);
		add(label);
		
		jFTextPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		jFTextPlaca.setEditable(false);
		jFTextPlaca.setToolTipText("Digite a placa no seguite formato: PLC-1234");
		jFTextPlaca.setBounds(146, 98, 86, 20);
		add(jFTextPlaca);
		
		JLabel label_1 = new JLabel("CPF do cliente");
		label_1.setBounds(10, 163, 80, 14);
		add(label_1);
		
		jFTextCPF = new JFormattedTextField(Mascara.mascaraCPF());
		jFTextCPF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if( !jFTextCPF.getText().replaceAll("[-._]", "").equals("") ){
					verificarCPF();
				}
			}
		});
		jFTextCPF.setBounds(146, 160, 105, 20);
		add(jFTextCPF);
		
		JButton btnListarCarros = new JButton("Listar Carros");
		btnListarCarros.setBounds(295, 97, 189, 23);
		btnListarCarros.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				SelecionaCarro.getInstance().escolheCarroVenda(VenderCarro.getInstance());
			}
		});
		add(btnListarCarros);
		
		JLabel lblValorDoCarro = new JLabel("Valor do carro");
		lblValorDoCarro.setBounds(10, 132, 80, 14);
		add(lblValorDoCarro);
		
		JButton btnVender = new JButton("Registrar Venda");
		btnVender.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				vender();
			}
		});
		btnVender.setBounds(342, 245, 142, 23);
		add(btnVender);
		
		textValorCarro = new JTextField();
		textValorCarro.setEditable(true);
		textValorCarro.setText("0");
		textValorCarro.setBounds(146, 129, 86, 20);
		add(textValorCarro);
		textValorCarro.setColumns(10);
	}
	
	public static VenderCarro getInstance(){
		if( vendercarro == null){
			vendercarro = new VenderCarro();
		}
		
		return vendercarro;
	}
	
	public <T> void carregaFormulario(T t){
		
		if(t instanceof Carro){
			carro = (Carro)t;
			this.jFTextPlaca.setText(carro.getPlaca());
			this.textValorCarro.setText(50000+random.nextInt(10000)+"");
		}
		
	}


	private void vender() {
		if( this.jFTextCPF == null ){
			super.setMensagemAlerta("Nenhum CPF foi adicionado � venda!");
			return;
		}
		if( this.jFTextPlaca == null){
			super.setMensagemAlerta("Nenhuma placa foi adicionado � venda!");
			return;
		}
				
		if (Controle.venderCarro(this.jFTextCPF.getText(), this.jFTextPlaca.getText(), this.textValorCarro.getText())){
			
			super.setMensagemConclusao("Venda feita com sucesso");
			Venda venda = new Venda(0, Calendar.getInstance(), Util.stringToDouble(textValorCarro.getText().replaceAll("[_.-]", "")));
			venda.setCarro(carro);
			venda.setCliente(Controle.getCliente(Util.StringToLong(jFTextCPF.getText().replaceAll("[_.-]", ""))));
						
			Principal.getInstance().trocaPanel(ExibirNota.getInstance());

			ExibirNota.getInstance().carregaNotaFiscal(this, venda);			
		}else{
			super.setMensagemErro("Erro na venda!");
		}
		
	}

	private void verificarCPF() {
		
		if( !Controle.verificarClienteCPF(this.jFTextCPF.getText()) ){
			super.setMensagemErro("Cliente n�o encontrado, venda n�o efetuada.");
		}
		
	}
	
	@Override
	public void apagaFormulario() {
		this.jFTextCPF.setText("");
		this.jFTextPlaca.setText("___-____");
		this.textValorCarro.setText("0");
		
		super.limpaMensagem();
	}
}
