package view.cadastros;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFormattedTextField;

import view.Funcionalidade;
import view.Login;
import view.Mascara;
import control.Controle;
import model.Funcionario;
import model.Util;

@SuppressWarnings("serial")
public class MantemFuncionario extends Funcionalidade {

	private static MantemFuncionario mantemFuncionario = null;
	private JTextField textFieldNome;
	private JTextField textFieldEndereco;
	private JPasswordField passwordField;
	private JPasswordField passwordFieldConfirmacao;
	
	private JButton btnSalvar;
	
	private JComboBox<String> comboBoxCargo;
	private JFormattedTextField formattedTextFieldCPF;
	private JFormattedTextField formattedTextFieldTelefone;
	
	private Funcionario funcionario;
	private JButton btnLimpaFormulrio;

	private MantemFuncionario() {
		super("Mantem Funcion\u00E1rio");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setBounds(10, 97, 107, 14);
		this.add(lblNewLabel_1);
		
		textFieldNome = new JTextField();
		textFieldNome.setBounds(158, 94, 266, 20);
		textFieldNome.setColumns(10);
		this.add(textFieldNome);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(10, 72, 107, 14);
		this.add(lblCpf);
		
		JLabel lblNewLabel_2 = new JLabel("Endere\u00E7o");
		lblNewLabel_2.setBounds(10, 125, 107, 14);
		this.add(lblNewLabel_2);
		
		textFieldEndereco = new JTextField();
		textFieldEndereco.setBounds(158, 122, 266, 20);
		textFieldEndereco.setColumns(10);
		this.add(textFieldEndereco);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(10, 150, 107, 14);
		this.add(lblTelefone);
		
		JLabel lblNewLabel_3 = new JLabel("Cargo");
		lblNewLabel_3.setBounds(10, 175, 107, 14);
		this.add(lblNewLabel_3);
		
		comboBoxCargo = new JComboBox<String>();
		comboBoxCargo.setBounds(158, 172, 86, 20);
		comboBoxCargo.setModel(new DefaultComboBoxModel<String>(new String[] {"agente", "gerente"}));
		comboBoxCargo.setSelectedIndex(0);
		comboBoxCargo.setToolTipText("");
		this.add(comboBoxCargo);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(10, 200, 107, 14);
		this.add(lblSenha);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(158, 197, 186, 20);
		passwordField.setColumns(10);
		this.add(passwordField);
		
		JLabel lblConfirmaoDeSenha = new JLabel("Confirma\u00E7\u00E3o de senha");
		lblConfirmaoDeSenha.setBounds(10, 225, 162, 14);
		this.add(lblConfirmaoDeSenha);
		
		passwordFieldConfirmacao = new JPasswordField();
		passwordFieldConfirmacao.setBounds(158, 222, 186, 20);
		this.add(passwordFieldConfirmacao);
		passwordFieldConfirmacao.setColumns(10);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salvarFuncionario();
			}
		});
		btnSalvar.setBounds(357, 290, 89, 23);
		this.add(btnSalvar);
		
		formattedTextFieldCPF = new JFormattedTextField(Mascara.mascaraCPF());
		formattedTextFieldCPF.setColumns(14);
		formattedTextFieldCPF.setBounds(158, 64, 107, 20);
		add(formattedTextFieldCPF);
		
		formattedTextFieldTelefone = new JFormattedTextField(Mascara.mascaraTelefone());
		formattedTextFieldTelefone.setColumns(15);
		formattedTextFieldTelefone.setBounds(158, 147, 107, 20);
		add(formattedTextFieldTelefone);
		
		JButton btnVerificar = new JButton("Verificar");
		btnVerificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verificaCPF();
			}
		});
		btnVerificar.setBounds(275, 63, 89, 23);
		add(btnVerificar);
		
		btnLimpaFormulrio = new JButton("Limpa Formul\u00E1rio");
		btnLimpaFormulrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				apagaFormulario();
			}
		});
		btnLimpaFormulrio.setBounds(158, 290, 142, 23);
		add(btnLimpaFormulrio);
	}
	
	public static MantemFuncionario getInstance(){
		if(mantemFuncionario == null){
			mantemFuncionario = new MantemFuncionario();
		}
		
		return mantemFuncionario;
	}
	
	public void apagaFormulario(){
		this.formattedTextFieldCPF.setText("");
		this.textFieldNome.setText("");
		this.textFieldEndereco.setText("");
		this.formattedTextFieldTelefone.setText("");
		this.comboBoxCargo.setSelectedIndex(0);
		this.passwordField.setText("");
		this.passwordFieldConfirmacao.setText("");
		
		this.formattedTextFieldCPF.setEditable(true);
		super.limpaMensagem();
	}
	
	public <T> void carregaFormulario(T t){
		Funcionario funcionario = (Funcionario)t;
		
		this.formattedTextFieldCPF.setText(""+funcionario.getCPF());
		this.formattedTextFieldCPF.setEditable(false);
		this.textFieldNome.setText(funcionario.getNome());
		this.textFieldEndereco.setText(funcionario.getEndereco());
		this.formattedTextFieldTelefone.setText(funcionario.getTelefone());
		for(int i=0; i < this.comboBoxCargo.getItemCount(); i++){
			if( funcionario.getCargo().equalsIgnoreCase(this.comboBoxCargo.getItemAt(i))){
				this.comboBoxCargo.setSelectedIndex(i);;
			}
		}
		this.passwordField.setText(funcionario.getSenhaSistema());
		this.passwordFieldConfirmacao.setText(funcionario.getSenhaSistema());
	}
	
	public Funcionario leFormulario(){

		this.funcionario = Login.getInstance().getFuncionario();
		
		long cpf = Util.StringToLong(this.formattedTextFieldCPF.getText().replaceAll("[_.-]", ""));
		Funcionario f = Controle.getFuncionario(cpf);
		
		long id = 0;
		
		if( f != null ){
			id = f.getId();
		}

		char[] temp = this.passwordFieldConfirmacao.getPassword();
		String senha = new String(temp);
		
		Funcionario funcionario = new Funcionario(	id,
													cpf,
													this.textFieldNome.getText(),
													this.textFieldEndereco.getText(),
													this.formattedTextFieldTelefone.getText(),
													this.comboBoxCargo.getItemAt(this.comboBoxCargo.getSelectedIndex()),
													senha );
		
		funcionario.setAgencia(this.funcionario.getAgencia());
		
		return funcionario;
	}
	
	public void verificaCPF(){
		long cpf = Util.StringToLong(this.formattedTextFieldCPF.getText().replaceAll("[_.-]", ""));
		Funcionario funcionario = Controle.getFuncionario(cpf);
		
		if( funcionario == null ){
			this.apagaFormulario();
			super.setMensagemErro("Nenhum funcionário encontrado!");
			
			this.formattedTextFieldCPF.setText(""+cpf);
			return;
		}
		
		this.carregaFormulario(funcionario);
		
		super.setMensagemConclusao("Dados carregados!");
	}
	
	public void salvarFuncionario(){
		Funcionario funcionario = leFormulario();
		
		if( Controle.salvarFuncionario(funcionario) ){
			super.setMensagemConclusao("Dados salvos!");
		}
		else{
			super.setMensagemErro("Erro ao salvar dados!");
		}
			
	}
}
