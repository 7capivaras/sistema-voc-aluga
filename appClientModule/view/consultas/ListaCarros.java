package view.consultas;

import java.util.ArrayList;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Lista;
import view.cadastros.MantemCarro;
import control.Controle;
import model.Carro;






import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaCarros extends Lista<Carro> {

	private static ListaCarros listaCarros = null;
	private ArrayList<Carro> carros;

	private ListaCarros() {
		super("Lista de Carros");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		String [] cabecalho = new String[]{"Marca","Modelo","Placa","Valor da di�ria"};
		super.setCabecalho(cabecalho);
		super.setModel();
		super.setSelecaoSigular();
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editar();
			}
		});
		btnEditar.setBounds(357, 291, 89, 23);
		add(btnEditar);
		
		JButton btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDados();
			}
		});
		btnCarregar.setBounds(229, 291, 89, 23);
		add(btnCarregar);
	}
	
	public static ListaCarros getInstance(){
		if(listaCarros == null){
			listaCarros = new ListaCarros();
		}
		
		return listaCarros;
	}
	
	private void setDados(){
		this.carros = Controle.getCarros();
		super.carregaDados(carros);
	}

	private void editar(){
		super.trocaTela(MantemCarro.getInstance());
	}
	
	protected String[] getTupla(Carro c){
		String [] tupla = new String[]{c.getMarca(), c.getModelo(), c.getPlaca(), c.getValorDiaria()+""};
		return tupla;
	}
}
