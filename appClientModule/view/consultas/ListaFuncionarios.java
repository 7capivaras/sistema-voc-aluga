package view.consultas;

import java.util.ArrayList;

import control.Controle;
import model.Funcionario;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Lista;
import view.cadastros.MantemFuncionario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaFuncionarios extends Lista<Funcionario> {
	
	private static ListaFuncionarios listaFuncionarios = null;
	private ArrayList<Funcionario> funcionarios;

	private ListaFuncionarios() {
		super("Lista de Funcionarios");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		String[] cabecalho = new String[]{"CPF","Nome","Cargo"};
		super.setCabecalho(cabecalho);
		super.setModel();
		super.setSelecaoSigular();
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editar();
			}
		});
		btnEditar.setBounds(357, 291, 89, 23);
		add(btnEditar);
		
		JButton btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDados();
			}
		});
		btnCarregar.setBounds(229, 291, 89, 23);
		add(btnCarregar);
		
	}
	
	public static ListaFuncionarios getInstance(){
		if(listaFuncionarios == null){
			listaFuncionarios = new ListaFuncionarios();
		}
		
		return listaFuncionarios;
	}
	
	protected void setDados(){
		this.funcionarios = Controle.getFuncionarios();
		super.carregaDados(this.funcionarios);
	}
	
	protected void editar(){
		super.trocaTela(MantemFuncionario.getInstance());
	}
	
	protected String[] getTupla(Funcionario f){
		String[] tupla = new String[]{ f.getCPF()+"", f.getNome(), f.getCargo() };
		return tupla;
	}
}
