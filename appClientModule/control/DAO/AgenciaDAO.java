package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import control.ConnectionFactory;
import model.Agencia;

public class AgenciaDAO {
	
	public static Agencia getAgenciaById(long id) {
		
		Agencia agencia = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Agencia where idagencia = ?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				agencia = getAgencia(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return agencia;
	}
	
	private static Agencia getAgencia(ResultSet resultSet) throws SQLException {
		Agencia agencia;
		agencia = new Agencia(  resultSet.getLong("idagencia"),
								resultSet.getString("nome"),
								resultSet.getString("endereco"),
								resultSet.getInt("capacidadecarros") );
		return agencia;
	}

}
