package view;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class Mascara {

		
	public static MaskFormatter criarMascara(String formato, char placeholder){
		
		MaskFormatter mascara = null;
		
		try{
			mascara = new MaskFormatter(formato);
			mascara.setPlaceholderCharacter(placeholder);
		}catch (ParseException e){
			 System.err.println("Erro na formatação: " + e.getMessage());
	         System.exit(-1);
		}
		
		return mascara;
	}
	
	public static MaskFormatter mascaraCartao(){
		return criarMascara("####  ####  ####  ####", '-');
	}
	
	public static MaskFormatter mascaraCRC(){
		return criarMascara("###", '-');
	}
	
	public static MaskFormatter mascaraChassi(){
		return criarMascara("AAAAAAAAAAAAAAAAA", '-');
	}
	
	public static MaskFormatter mascaraTelefone(){
		return criarMascara("(##)###-###-###", '_');
	}
	
	public static MaskFormatter mascaraPlaca() {
		
		return criarMascara("UUU-####", '_');
	
	}
	
	public static MaskFormatter mascaraData() {
				 
		 return criarMascara("##/##/####", '_');
	}
	
	public static MaskFormatter mascaraValidadeCartao() {
		
		 return criarMascara("##/####", '_');
	}
	
	public static MaskFormatter mascaraCPF() {
		
		return criarMascara("###.###.###-##", '_');
	
	}
		
}
