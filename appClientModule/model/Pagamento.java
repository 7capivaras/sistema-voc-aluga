package model;
import java.util.Calendar;


public class Pagamento
{
	private long idPagamento;
	private double valor;
	private Calendar dataPagamento;
	private double taxaRetorno;
	private double taxaDanificacao;
	private double taxaAtraso;
	private double desconto;
	private String formaPagamento;

	private Locacao locacao;
	
	public Pagamento( 	long id,
						double valor,
						Calendar dataPagamento,
						double taxaRetorno,
						double taxaDanificacao,
						double taxaAtraso,
						double desconto,
						String formaPagamento )
	{
		this.idPagamento = id;
		this.valor = valor;
		this.dataPagamento = dataPagamento;
		this.taxaRetorno = taxaRetorno;
		this.taxaDanificacao = taxaDanificacao;
		this.taxaAtraso = taxaAtraso;
		this.desconto = desconto;
		this.formaPagamento = formaPagamento;
		
		this.locacao = null;
	}
	
	public double valorTotal(){
		double total = this.taxaAtraso + this.taxaDanificacao 
				+ this.taxaRetorno + this.valor - this.desconto;
		return total;
	}
	
	public double totalDeTaxas(){
		double taxas = this.taxaAtraso + this.taxaDanificacao 
				+ this.taxaRetorno - this.desconto;
		return taxas;
	}

	public Locacao getLocacao() {
		return locacao;
	}

	public void setLocacao(Locacao locacao) {
		this.locacao = locacao;
	}

	public long getIdPagamento() {
		return idPagamento;
	}

	public double getValor() {
		return valor;
	}

	public double getDesconto() {
		return desconto;
	}

	public Calendar getDataPagamento() {
		return dataPagamento;
	}

	public double getTaxaRetorno() {
		return taxaRetorno;
	}

	public double getTaxaDanificacao() {
		return taxaDanificacao;
	}

	public double getTaxaAtraso() {
		return taxaAtraso;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}
}

