package control;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConnectionFactory {

	private String urlBD;
	private String usuarioBD;
	private String senhaBD;
	
	public Connection getConnection(){

		try {
				carregaConfiguracoesDB();
				return DriverManager.getConnection(urlBD,usuarioBD, senhaBD);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		return null;
		
	}
	
	public void carregaConfiguracoesDB(){
				
		try{

			String arquivoConfiguracoes = "database_settings.xml";
			File fXmlFile = new File(arquivoConfiguracoes);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("database");
			
			for (int temp = 0; temp < nList.getLength(); temp++) {
				 
				Node nNode = nList.item(temp);
		 
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
		 
					if( eElement.getAttribute("id").equals("postgresql")){						
						urlBD = eElement.getElementsByTagName("url").item(0).getTextContent();
						usuarioBD = eElement.getElementsByTagName("user").item(0).getTextContent();
						senhaBD = eElement.getElementsByTagName("password").item(0).getTextContent();						
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}