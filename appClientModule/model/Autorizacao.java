package model;

import java.sql.Date;


public class Autorizacao
{
	private Motorista motorista;
	private Locacao locacao;
	
	public Autorizacao(Motorista motorista, Locacao locacao){
		this.motorista = motorista;
		this.locacao = locacao;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public Locacao getLocacao() {
		return locacao;
	}

}

