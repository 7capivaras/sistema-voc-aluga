package model;
import java.util.Calendar;

public class Carro
{

	private long id;
	private String chassi;
	private String marca;	
	private String modelo;
	private String descricao;
	private Calendar dataAquisicao;
	private double kmRodados;
	private Calendar ultimaRevisao;
	private String placa;
	private double valorDiaria;
	
	private Reserva reserva;
	private Manutencao manutencao;
	private Agencia agencia;
	
	
	public Carro(){
		super();
	}
	
	public Carro(   long id,
					String chassi,
					String marca,
					String modelo,
					String descricao,
					Calendar dataAquisicao,
					double kmRodados,
					Calendar ultimaRevisao,
					String placa,
					double valorDiaria )
	{
		this.id = id;
		this.chassi = chassi;
		this.marca = marca;
		this.modelo = modelo;
		this.descricao = descricao;
		this.dataAquisicao = dataAquisicao;
		this.kmRodados = kmRodados;
		this.ultimaRevisao = ultimaRevisao;
		this.placa = placa;
		this.valorDiaria = valorDiaria;
		this.reserva = null;
		this.manutencao = null;
		this.agencia = null;
	}
	
	

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}
	
	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public void setManutencao(Manutencao manutencao) {
		this.manutencao = manutencao;
	}

	public void setKmRodados(double kmRodados) {
		this.kmRodados = kmRodados;
	}

	public long getId() {
		return id;
	}

	public String getChassi() {
		return chassi;
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getDescricao() {
		return descricao;
	}

	public Calendar getDataAquisicao() {
		return dataAquisicao;
	}

	public double getKmRodados() {
		return kmRodados;
	}

	public Calendar getUltimaRevisao() {
		return ultimaRevisao;
	}

	public String getPlaca() {
		return placa;
	}

	public double getValorDiaria() {
		return valorDiaria;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public Manutencao getManutencao() {
		return manutencao;
	}

	public Agencia getAgencia() {
		return agencia;
	}
}