package model;
import java.util.ArrayList;
import java.util.Calendar;


public class Cliente
{
	
	private long id;
	private long cpf;	
	private String nome;
	private String endereco;
	private String telefone;
	private Calendar nascimento;
	private boolean listaNegra;
	
	private ArrayList<Locacao> locacoes;
	private ArrayList<Reserva> reservas;
	
	public Cliente(	long id,
					long cpf,
					String nome,
					String endereco,
					String telefone,
					Calendar nascimento )
	{
		this.id = id;
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
		this.nascimento = nascimento;
		
		this.listaNegra = false;
		
		this.locacoes = new ArrayList<Locacao>();
		this.reservas = new ArrayList<Reserva>();
	}
	
	public void addLocacao(Locacao l){
		this.locacoes.add(l);
	}
	
	public void addReserva(Reserva r){
		this.reservas.add(r);
	}

	public ArrayList<Locacao> getLocacoes() {
		return locacoes;
	}

	public ArrayList<Reserva> getReservas() {
		return reservas;
	}

	public boolean isListaNegra() {
		return listaNegra;
	}

	public void setListaNegra(boolean listaNegra) {
		this.listaNegra = listaNegra;
	}

	public long getId() {
		return id;
	}

	public long getCpf() {
		return cpf;
	}

	public String getNome() {
		return nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public Calendar getNascimento() {
		return nascimento;
	}

}

