package view.pagamento;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Lista;
import view.Principal;
import model.Locacao;
import model.Util;

@SuppressWarnings("serial")
public class PendenciasCliente extends Lista<Locacao>{
	
	private static PendenciasCliente listaPendencias = null;
	private ArrayList<Locacao> pendencias;
	private Funcionalidade funcionalidade;
	
	public PendenciasCliente() {
		super("Lista de Pend�ncias");
		funcionalidade = null;
		String[] cabecalho = new String[]{	"Nome Cliente",
											"Placa Carro",
											"Data da Loca��o",
											"Previsao de Entrega"};
		super.setCabecalho(cabecalho);
		super.setModel();
		super.setSelecaoSigular();
		
		JButton btnPagar = new JButton("Pagar");
		btnPagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pagar();
			}
		});
		btnPagar.setBounds(353, 280, 89, 23);
		add(btnPagar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		btnCancelar.setBounds(224, 280, 89, 23);
		add(btnCancelar);
	}
	
	protected void cancelar() {
		Principal.getInstance().trocaPanel(this.funcionalidade);
	}

	protected void pagar() {
		super.trocaTela(this.funcionalidade);
	}

	public static PendenciasCliente getInstance(){
		if(listaPendencias == null){
			listaPendencias = new PendenciasCliente();
		}
		
		return listaPendencias;
	}

	protected void setDados(Funcionalidade f, ArrayList<Locacao> locacoes) {
		this.funcionalidade = f;
		this.pendencias = locacoes;
		super.carregaDados(pendencias);
	}
	
	protected String[] getTupla(Locacao p){
		String[] tupla = new String[]{	p.getCliente().getNome(), 
										p.getCarro().getPlaca(),
										Util.calendarToString(p.getDataLocacao()),
										Util.calendarToString(p.getPrevisaoEntrega()) };
		return tupla;
	}
}
