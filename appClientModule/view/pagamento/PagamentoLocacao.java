package view.pagamento;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import view.Funcionalidade;
import view.Login;
import view.Mascara;
import view.Principal;
import model.Cliente;
import model.Locacao;
import model.Pagamento;
import model.Util;
import control.Controle;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class PagamentoLocacao extends Funcionalidade {

	private static PagamentoLocacao pagamento = null;
	
	private JLabel lblPlacaInfo;
	private JLabel lblValorDiariaInfo;
	private JLabel lblValorTotal;
	private JLabel lblSugestaoTaxaAtraso;
	private JLabel lblSugestaoDesconto;
	
	private JFormattedTextField jFTextFieldCpf;
	private JTextField textFieldTaxaAtraso;
	private JTextField textFieldTaxaRetorno;
	private JTextField textFieldAdiantamento;
	private JTextField textFieldTaxaDanificacao;
	
	private JComboBox<String> comboBoxFormaPagamento;
	
	private Locacao locacao;
	
	private PagamentoLocacao() {
		super("Pagamento de Loca��o");
		
		this.locacao = null;
		this.initLabels();
		this.initTextField();
		this.initComboBoxs();
		this.initButtons();
		
	}
	
	public static PagamentoLocacao getInstance(){
		if( pagamento == null){
			pagamento = new PagamentoLocacao();
		}
		
		return pagamento;
	}
	
	private void initLabels(){
		
		JLabel lblCpfCliente = new JLabel("CPF do Cliente:");
		lblCpfCliente.setBounds(59, 65, 92, 14);
		add(lblCpfCliente);
		
		JLabel lblPlaca = new JLabel("Placa:");
		lblPlaca.setBounds(69, 95, 40, 14);
		add(lblPlaca);
		
		lblPlacaInfo = new JLabel("___-____");
		lblPlacaInfo.setBounds(115, 95, 60, 14);
		add(lblPlacaInfo);
		
		JLabel lblValorDaDiria = new JLabel("Valor da Di\u00E1ria:");
		lblValorDaDiria.setBounds(218, 95, 86, 14);
		add(lblValorDaDiria);
		
		lblValorDiariaInfo = new JLabel("R$ 0");
		lblValorDiariaInfo.setBounds(314, 95, 60, 14);
		add(lblValorDiariaInfo);
		
		JLabel lblTaxaDeAtraso = new JLabel("Taxa de Atraso:");
		lblTaxaDeAtraso.setBounds(10, 148, 119, 14);
		add(lblTaxaDeAtraso);
		
		JLabel lblTaxaDeRetorno = new JLabel("Taxa de Retorno:");
		lblTaxaDeRetorno.setBounds(260, 148, 114, 14);
		add(lblTaxaDeRetorno);
		
		JLabel lblDescontoPorAdiantamento = new JLabel("Desconto:");
		lblDescontoPorAdiantamento.setBounds(10, 204, 107, 14);
		add(lblDescontoPorAdiantamento);
		
		JLabel lblTaxaDeDanificao = new JLabel("Taxa de Danifica\u00E7\u00E3o:");
		lblTaxaDeDanificao.setBounds(260, 204, 128, 14);
		add(lblTaxaDeDanificao);
		
		JLabel lblFormaDePagamento = new JLabel("Forma de Pagamento:");
		lblFormaDePagamento.setBounds(10, 240, 141, 14);
		add(lblFormaDePagamento);
		
		JLabel lblTotalPagar = new JLabel("Total \u00E0 Pagar:");
		lblTotalPagar.setBounds(260, 240, 79, 14);
		add(lblTotalPagar);
		
		lblValorTotal = new JLabel("R$ 0");
		lblValorTotal.setBounds(349, 240, 86, 14);
		add(lblValorTotal);
		
		lblSugestaoTaxaAtraso = new JLabel("R$ 0");
		lblSugestaoTaxaAtraso.setBounds(164, 128, 86, 14);
		add(lblSugestaoTaxaAtraso);
		
		lblSugestaoDesconto = new JLabel("R$ 0");
		lblSugestaoDesconto.setBounds(164, 185, 86, 14);
		add(lblSugestaoDesconto);
		
	}
	
	private void initTextField(){
		
		jFTextFieldCpf = new JFormattedTextField(Mascara.mascaraCPF());
		jFTextFieldCpf.setBounds(148, 62, 102, 20);
		add(jFTextFieldCpf);
		
		textFieldTaxaAtraso = new JTextField();
		textFieldTaxaAtraso.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calculaTotalAPagar();
			}
		});
		textFieldTaxaAtraso.setBounds(164, 145, 86, 20);
		add(textFieldTaxaAtraso);
		textFieldTaxaAtraso.setColumns(10);
		
		textFieldTaxaRetorno = new JTextField();
		textFieldTaxaRetorno.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calculaTotalAPagar();
			}
		});
		textFieldTaxaRetorno.setBounds(387, 145, 86, 20);
		add(textFieldTaxaRetorno);
		textFieldTaxaRetorno.setColumns(10);
		
		textFieldAdiantamento = new JTextField();
		textFieldAdiantamento.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calculaTotalAPagar();
			}
		});
		textFieldAdiantamento.setBounds(162, 201, 88, 20);
		add(textFieldAdiantamento);
		textFieldAdiantamento.setColumns(10);
		
		textFieldTaxaDanificacao = new JTextField();
		textFieldTaxaDanificacao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calculaTotalAPagar();
			}
		});
		textFieldTaxaDanificacao.setBounds(387, 201, 86, 20);
		add(textFieldTaxaDanificacao);
		textFieldTaxaDanificacao.setColumns(10);
	}

	private void initButtons(){
		
		JButton btnBuscarLocacoes = new JButton("Buscar Loca\u00E7\u00F5es");
		btnBuscarLocacoes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscaLocacoesAbertas();
			}
		});
		btnBuscarLocacoes.setBounds(264, 61, 151, 23);
		add(btnBuscarLocacoes);
		
		JButton btnVisualizarNotaFiscal = new JButton("Pagar");
		btnVisualizarNotaFiscal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pagar();
			}
		});
		btnVisualizarNotaFiscal.setBounds(371, 284, 102, 23);
		add(btnVisualizarNotaFiscal);
	}

	private void initComboBoxs(){
		comboBoxFormaPagamento = new JComboBox<String>();
		comboBoxFormaPagamento.setModel(new DefaultComboBoxModel<String>(new String[] {"Dinheiro", "Cr\u00E9dito", "D\u00E9bito"}));
		comboBoxFormaPagamento.setBounds(164, 237, 86, 20);
		add(comboBoxFormaPagamento);
	}
	
	private void pagar() {
		double taxaAtraso = 0;
		double descontoAdiantamento = 0;
		double taxaRetorno = 0;
		double taxaDanificacao = 0;
		
		double totalDiarias = 0;
		
		if(this.locacao.haTaxaDanificacao())
			taxaDanificacao = Util.stringToDouble(this.textFieldTaxaDanificacao.getText());
		
		if(this.locacao.haTaxaRetorno(Login.getInstance().getFuncionario().getAgencia()))
			taxaRetorno = Util.stringToDouble(this.textFieldTaxaRetorno.getText());
		
		if(this.locacao.haDescontoDeAdiantamento())
			descontoAdiantamento = Util.stringToDouble(this.textFieldAdiantamento.getText());
		
		if(this.locacao.haTaxaAtraso())
			taxaAtraso = Util.stringToDouble(this.textFieldTaxaAtraso.getText());
		
		totalDiarias = this.locacao.tempoDeAluguel() * this.locacao.getCarro().getValorDiaria();
		
		for(Pagamento p: this.locacao.getPagamento()){
			totalDiarias -= p.getValor();
		}
		
		double totalAPagar = totalDiarias + taxaAtraso + taxaDanificacao + taxaRetorno - descontoAdiantamento;
		
		if(totalAPagar == 0){
			super.setMensagemAlerta("O pagamento de R$ 0 n�o pode ser efetivado!");
			return;
		}
		
		Pagamento p = new Pagamento(	0,
										totalDiarias,
										Calendar.getInstance(),
										taxaRetorno,
										taxaDanificacao,
										taxaAtraso,
										descontoAdiantamento,
										this.comboBoxFormaPagamento.getSelectedItem().toString());
		p.setLocacao(this.locacao);
		if(Controle.registraPagamentoLocacao(p)){
			ExibirNota.getInstance().carregaNotaFiscal(this, p);
		}
		else
			super.setMensagemErro("Falha no registro do pagamento!");
	}
	
	private void buscaLocacoesAbertas() {
		
		if(this.jFTextFieldCpf.getText().replaceAll("[_.-]", "").equals("")){
			super.setMensagemErro("CPF inv�lido!");
			return;
		}
		
		long cpf = Util.StringToLong(this.jFTextFieldCpf.getText().replaceAll("[_.-]", ""));
		Cliente cliente = Controle.getCliente(cpf);
		
		if( cliente == null ){
			super.setMensagemErro("CPF n�o cadastrado!");
			return;
		}
		
		cliente = Controle.getPendencias(cliente);
		
		if(cliente.getLocacoes().size() == 0){
			super.setMensagemAlerta("Nenhuma pend�ncia encontrada para o CPF informado!");
			return;
		}

		Principal.getInstance().trocaPanel(PendenciasCliente.getInstance());
		PendenciasCliente.getInstance().setDados(this,cliente.getLocacoes());
	}
	
	public <T> void carregaFormulario(T t){
		
		if( !(t instanceof Locacao) )
			return;
		
		this.locacao = (Locacao)t;
		this.locacao = Controle.getPagamentosLocacao(this.locacao);
		this.jFTextFieldCpf.setText(locacao.getCliente().getCpf()+"");
		this.lblPlacaInfo.setText(locacao.getCarro().getPlaca());
		this.lblValorDiariaInfo.setText(locacao.getCarro().getValorDiaria()+"");
		
		this.calculaTaxas();
		super.setMensagemConclusao("Loca��o carregada para pagamento!");
	}
	
	private void verificaTaxas(){
		if(this.locacao.haTaxaAtraso()){ this.textFieldTaxaAtraso.setEnabled(true); }
		else { this.textFieldTaxaAtraso.setEnabled(false); }
		
		if(this.locacao.haDescontoDeAdiantamento()){ this.textFieldAdiantamento.setEnabled(true);}
		else { this.textFieldAdiantamento.setEnabled(false); }
		
		if(this.locacao.haTaxaDanificacao()) { this.textFieldTaxaDanificacao.setEnabled(true);}
		else{ this.textFieldTaxaDanificacao.setEnabled(false); }
		
		if(this.locacao.haTaxaRetorno(Login.getInstance().getFuncionario().getAgencia()))
		{ this.textFieldTaxaRetorno.setEnabled(true);}
		else{ this.textFieldTaxaRetorno.setEnabled(false); }
	}
	
	private void calculaTotalAPagar(){
		
		double taxaAtraso = 0;
		double descontoAdiantamento = 0;
		double taxaRetorno = 0;
		double taxaDanificacao = 0;
		
		double totalDiarias = 0;
		
		if(this.locacao.haTaxaDanificacao())
			taxaDanificacao = Util.stringToDouble(this.textFieldTaxaDanificacao.getText());
		
		if(this.locacao.haTaxaRetorno(Login.getInstance().getFuncionario().getAgencia()))
			taxaRetorno = Util.stringToDouble(this.textFieldTaxaRetorno.getText());
		
		if(this.locacao.haDescontoDeAdiantamento())
			descontoAdiantamento = Util.stringToDouble(this.textFieldAdiantamento.getText());
		
		if(this.locacao.haTaxaAtraso())
			taxaAtraso = Util.stringToDouble(this.textFieldTaxaAtraso.getText());
		
		totalDiarias = this.locacao.tempoDeAluguel() * this.locacao.getCarro().getValorDiaria();
		
		for(Pagamento p: this.locacao.getPagamento()){
			totalDiarias -= p.getValor();
		}
		
		double totalAPagar = totalDiarias + taxaAtraso + taxaDanificacao + taxaRetorno - descontoAdiantamento;
		
		this.lblValorTotal.setText("R$ "+totalAPagar);
		
	}
	
	private void calculaTaxas(){
		if(this.locacao == null)
			return;
		
		this.verificaTaxas();
		
		if(this.textFieldTaxaDanificacao.isEnabled())
			this.textFieldTaxaDanificacao.setText("0");
		
		if(this.textFieldTaxaRetorno.isEnabled())
			this.textFieldTaxaRetorno.setText("0");
		
		
		double taxaAtraso = this.locacao.tempoDeAtraso() 
				* this.locacao.getCarro().getValorDiaria();
		
		double descontoAdiantamento = this.locacao.tempoDeAdiantamento() 
				* this.locacao.getCarro().getValorDiaria();
		
		for(Pagamento p: this.locacao.getPagamento()){
			taxaAtraso -= p.getTaxaAtraso();
		}
		
		if(taxaAtraso < 0){
			taxaAtraso = 0;
		}
		
		if(this.locacao.haDescontoDeAdiantamento()){
			this.lblSugestaoDesconto.setText("R$ "+descontoAdiantamento);
			this.textFieldAdiantamento.setText(""+descontoAdiantamento);
		}
		
		if(this.locacao.haTaxaAtraso()){
			this.lblSugestaoTaxaAtraso.setText("R$ "+taxaAtraso);
			this.textFieldTaxaAtraso.setText(""+taxaAtraso);
		}
		
		this.calculaTotalAPagar();
	}

	@Override
	public void apagaFormulario() {
		this.lblPlacaInfo.setText("___-____");
		this.lblValorDiariaInfo.setText("R$ 0");
		this.lblValorTotal.setText("R$ 0");
		this.lblSugestaoTaxaAtraso.setText("R$ 0");
		this.lblSugestaoDesconto.setText("R$ 0");
		
		this.jFTextFieldCpf.setText("");
		this.textFieldTaxaAtraso.setText("");
		this.textFieldTaxaAtraso.setEnabled(false);
		this.textFieldTaxaRetorno.setText("");
		this.textFieldTaxaRetorno.setEnabled(false);
		this.textFieldAdiantamento.setText("");
		this.textFieldAdiantamento.setEnabled(false);
		this.textFieldTaxaDanificacao.setText("");
		this.textFieldTaxaDanificacao.setEnabled(false);
		
		this.comboBoxFormaPagamento.setSelectedIndex(0);
		
		super.limpaMensagem();
	}
}
