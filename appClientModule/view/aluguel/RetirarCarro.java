package view.aluguel;

import model.Autorizacao;
import model.Locacao;
import model.Motorista;
import model.Util;
import view.Lista;
import view.Mascara;

import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import control.Controle;
import control.DAO.AutorizacaoDAO;
import control.DAO.MotoristaDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class RetirarCarro extends Lista<Locacao> {

	private static RetirarCarro retirarCarro = null;
	
	private JFormattedTextField jFTextFieldCpf;
	
	private RetirarCarro() {
		super("Retirada de Carro");
		
		jFTextFieldCpf = new JFormattedTextField(Mascara.mascaraCPF());
		jFTextFieldCpf.setBounds(165, 59, 111, 20);
		add(jFTextFieldCpf);
		
		JButton btnBuscarLocaes = new JButton("Buscar Loca\u00E7\u00F5es");
		btnBuscarLocaes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarLocacoes();
			}
		});
		btnBuscarLocaes.setBounds(297, 58, 150, 23);
		add(btnBuscarLocaes);
		
		JLabel lblCpfDoMotorista = new JLabel("CPF do motorista:");
		lblCpfDoMotorista.setBounds(53, 62, 102, 14);
		add(lblCpfDoMotorista);
		
		JButton btnRegistrarRetirada = new JButton("Registrar Retirada");
		btnRegistrarRetirada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				registrarRetirada();
			}
		});
		btnRegistrarRetirada.setBounds(297, 264, 150, 23);
		add(btnRegistrarRetirada);
		String[] cabecalho = new String[]{	"Nome Agencia",
				"Nome Cliente", 
				"Data Loca��o", 
				"Data Retirada", 
				"Previs�o Entrega"};
		super.setCabecalho(cabecalho);
		super.setSelecaoSigular();
	}
	
	protected void registrarRetirada() {
		Locacao locacao = super.getDadoSelecionado();
		Motorista motorista = Controle.getMotoristaByCpf(Util.StringToLong(jFTextFieldCpf.getText().replaceAll("[_.-]", "")));
		
		Autorizacao autorizacao = new Autorizacao(motorista, locacao);
		if(Controle.registraRetirada(autorizacao))
			super.setMensagemConclusao("Retirada efetuada com sucesso!");
		else
			super.setMensagemErro("Deu erro.");
		
	}

	protected void buscarLocacoes() {
		if(jFTextFieldCpf.getText().replaceAll("[_.-]", "").equals("")){
			super.setMensagemErro("Digite o cpf!");
		}
		else{
			ArrayList<Locacao> locacoes = Controle.buscarLocacoes(jFTextFieldCpf.getText().replaceAll("[_.-]", ""));
			if(locacoes != null){
				if(locacoes.size() == 0)
					super.setMensagemAlerta("N�o foram encontradas loca��es para esse cpf.");
				else{
					super.carregaDados(locacoes);
				}
			}
		}
		
	}

	public static RetirarCarro getInstance(){
		if(retirarCarro == null){
			retirarCarro = new RetirarCarro();
		}
		
		return retirarCarro;
	}

	@Override
	protected String[] getTupla(Locacao l){
		String[] tupla = new String[]{ 	l.getAgencia().getNome()+"", 
										l.getCliente().getNome()+"", 
										Util.calendarToString(l.getDataLocacao())+"",  
										Util.calendarToString(l.getDataRetirada())+"",
										Util.calendarToString(l.getPrevisaoEntrega())+""};
		return tupla;
	}
}
