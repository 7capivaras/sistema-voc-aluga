package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import control.ConnectionFactory;
import model.Reserva;
import model.Util;

public class ReservaDAO {

	public static boolean reservarCarro(long CPF, long idCarro, long idCliente, Calendar dataRetirada, 
										Calendar validadeReserva, long idFuncionario) {
		
		java.sql.Date dataAtual = Util.calendarToSQL(Calendar.getInstance());
		java.sql.Date dataRetiradaSql = Util.calendarToSQL(dataRetirada);
		java.sql.Date validadeReservaSql = Util.calendarToSQL(validadeReserva);	
		
		boolean concluido = false;
		
		if (dataAtual == null || validadeReservaSql == null || dataRetiradaSql == null)	
			return concluido;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("insert into voce_aluga.Reserva(dataReserva, dataRetirada, validadeReserva, carro_id, cliente_id, funcionario_id)"
					+ " values (?, ?, ?, ?, ?, ?)");
			
			stmt.setDate(1, dataAtual);
			stmt.setDate(2, dataRetiradaSql);
			stmt.setDate(3, validadeReservaSql);
			stmt.setLong(4, idCarro);
			stmt.setLong(5, idCliente);
			stmt.setLong(6, idFuncionario );

			stmt.execute();
			stmt.close();
			concluido = true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
		
	}
	
	public static boolean haReserva (Calendar dataRetirada, Calendar validadeReserva, long idCarro){
		
		boolean haReserva = false;
		
		java.sql.Date dataInicio = Util.calendarToSQL(dataRetirada);
		java.sql.Date dataFim = Util.calendarToSQL(validadeReserva);
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Reserva where carro_id = ? and "
					+ "(dataretirada BETWEEN ? and ?) or (validadereserva BETWEEN ? and ?)");
			stmt.setLong(1, idCarro);
			stmt.setDate(2, dataInicio);
			stmt.setDate(3, dataFim);
			stmt.setDate(4, dataInicio);
			stmt.setDate(5, dataFim);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				haReserva = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return haReserva;
	}
	
	public static boolean haReserva(String CPF){
		
		boolean haReserva = false;
		
		if(CPF.equals("") || CPF == null)
			return haReserva;
		
		long cpf = Util.StringToLong(CPF);
		java.sql.Date dataAtual = Util.calendarToSQL(Calendar.getInstance());
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Reserva where cliente_id = "
					+ "(select id from voce_aluga.Cliente where cpf = ?)"
					+ "and dataretirada = ?");
			stmt.setLong(1, cpf);
			stmt.setDate(2, dataAtual);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				haReserva = true;
			}
			
		} catch (SQLException e) {		
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return haReserva;
	}
	
	public ArrayList<Reserva> getReservas(){
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		Reserva reserva = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.reserva;");
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				reserva = getReserva(resultSet);
				reservas.add(reserva);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(reservas.size() == 0){
			return null;
		}
		return reservas;
	}
	
	public static boolean removeReserva(long id){
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("delete from voce_aluga.reserva where idreserva = ?;");
			stmt.setLong(1, id);
			stmt.execute();
			con.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	private static Reserva getReserva(ResultSet resultSet){
		Reserva reserva = null;
		try{
			reserva = new Reserva(	resultSet.getLong("idreserva"),
									Util.stringSQLToCalendar(resultSet.getString("datareserva")),
									Util.stringSQLToCalendar(resultSet.getString("dataretirada")),
									Util.stringSQLToCalendar(resultSet.getString("validadereserva")) );
			
			reserva.setCarro(CarroDAO.getCarroById(resultSet.getLong("carro_id")));
			reserva.setCliente(ClienteDAO.getClienteById(resultSet.getLong("cliente_id")));
			reserva.setFuncionario(FuncionarioDAO.getFuncionarioById(resultSet.getLong("funcionario_id")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reserva;
	}

	public static ArrayList<Reserva> getReservasAbertas() {
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		Reserva reserva = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.reserva where current_date <= dataretirada;");
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				reserva = getReserva(resultSet);
				reservas.add(reserva);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(reservas.size() == 0){
			return null;
		}
		return reservas;
	}
	
}
