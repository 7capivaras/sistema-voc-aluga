﻿SELECT setval( 'voce_aluga.agencia_idAgencia_seq', 1 );

INSERT INTO voce_aluga.agencia(
            idAgencia, nome, capacidadecarros, endereco)
    VALUES (1, 'Val verde', 50, 'Rua 45'),
	   (2, 'Coral largo', 50, 'Rua 293');

SELECT setval( 'voce_aluga.funcionario_id_seq', 1 );

INSERT INTO voce_aluga.funcionario(
            id, cargo, cpf, endereco, nome, senhasistema, telefone, agencia_id)
    VALUES ( 1, 'gerente', 11111111111, 'Rua 8', 'Administrador', 'bob', '21999999999', 1);
    
INSERT INTO voce_aluga.funcionario(
            cargo, cpf, endereco, nome, senhasistema, telefone, agencia_id)
    VALUES ('gerente', 12345678810, 'Rua 12', 'Aluizio', 'boru', '21999999999', 1),
	   ('gerente', 12345678739, 'Rua 23', 'Amanda', 'boru', '21999999999', 1),
           ('agente', 12345678658, 'Rua 22', 'Rafael', 'boru', '21999999999', 1),
           ('agente', 12345678577, 'Rua 21', 'Yves', 'boru', '21999999999', 1);

SELECT setval( 'voce_aluga.cliente_id_seq', 1 );

INSERT INTO voce_aluga.cliente(
            id, cpf, endereco, nascimento, nome, telefone)
    VALUES (1, 12345678909, 'Copacabana Palace', '04/09/1981', 'Beyoncé Giselle Knowles-Carter', '21999999999');

INSERT INTO voce_aluga.cliente(
            cpf, endereco, nascimento, nome, telefone)
    VALUES (12345678900, 'Village Palace', '20/05/1952', 'Wallace Mattos', '21988888888'),
	   (12345678901, 'São bento lemos', '06/04/1984', 'Joyce Coutrinho', '22977777777'),
	   (12345678902, 'Rua raimundo dantas', '07/07/1977', 'Jorge Armando', '23966666666'),
	   (12345678903, 'Av. Rio Branco', '16/08/1975', 'Fernando Litio', '22955555555'),
	   (12345678904, 'Rua Uruguaiana', '26/11/1963', 'Thiago Bastos', '22957684321');


SELECT setval( 'voce_aluga.carro_id_seq', 1 );

INSERT INTO voce_aluga.carro(
            id, chassi, dataaquisicao, descricao, kmrodados, marca, modelo, 
            placa, ultimarevisao, valordiaria, agencia_id)
    VALUES (1, 'fa89f6a98df7a9s7d', '20/10/2014', 'bonito', 50.0, 'Pagani', 'Zonda R', 'PLC-1234', '21/10/2014', 1000.0, 1);

INSERT INTO voce_aluga.carro(
            chassi, dataaquisicao, descricao, kmrodados, marca, modelo, 
            placa, ultimarevisao, valordiaria, agencia_id)
    VALUES ('fa89f6a98df7a9s7e', '20/10/2014', 'bonito', 50.0, 'Pagani', 'Zonda Cinque', 'PLC-1111', '21/10/2014', 900.0, 1),
           ('fa89f6a98df7a9s45', '20/10/2014', 'Lindo', 50.0, 'Lamborghini', 'Asterion LPI 910-4', 'PLC-2222', '21/10/2014', 700.0, 1),
           ('fa89f6a98df7a9s87', '20/10/2014', 'Lindo', 50.0, 'Lamborghini', 'Aventador LP 700-4', 'PLC-3333', '21/10/2014', 700.0, 1),
           ('fa89f6a98df7a9s67', '20/10/2014', 'Lindo', 50.0, 'Lamborghini', 'Huracan LP 610-4', 'PLC-4444', '21/10/2014', 700.0, 1),
           ('fa89f6a98df7a9s62', '20/10/2014', 'Roadster', 50.0, 'Lamborghini', 'Veneno Roadster', 'PLC-5555', '21/10/2014', 700.0, 1);


SELECT setval( 'voce_aluga.cartaocredito_id_seq', 1 );
            
INSERT INTO voce_aluga.cartaocredito(
	    id, crc, nome, numero, validade)
    VALUES (1, 111, 'aleatorio', 123412341234, '2017-11-01');


SELECT setval( 'voce_aluga.locacao_idlocacao_seq', 1 );

INSERT INTO voce_aluga.locacao(
            idlocacao, datalocacao, dataentrega, dataretirada, previsaoentrega, agencia_id, cartaocredito_id, 
            cliente_id, carro_id)
    VALUES (1, '2014-11-02', null, '2014-11-03', '2014-11-06', 1, 1, 1, 1);

INSERT INTO voce_aluga.locacao(
             datalocacao, dataentrega, dataretirada, previsaoentrega, agencia_id, cartaocredito_id, 
            cliente_id, carro_id)
    VALUES (current_date - interval '1 days' , null, current_date + interval '1 days', current_date + interval '5 days', 1, 1, 2, 2),
	   (current_date - interval '5 days' , null, current_date - interval '2 days', current_date + interval '2 days', 1, 1, 3, 3),
	   (current_date - interval '7 days' , null, current_date - interval '2 days', current_date + interval '1 days', 1, 1, 4, 4),
	   (current_date - interval '8 days' , current_date - interval '5 days', current_date - interval '7 days', current_date - interval '3 days', 1, 1, 5, 5),
	   (current_date - interval '9 days' , current_date - interval '1 days', current_date - interval '8 days', current_date - interval '6 days', 1, 1, 6, 6);
