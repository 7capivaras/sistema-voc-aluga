package view.cadastros;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import view.Funcionalidade;
import view.Mascara;
import control.Controle;
import model.Util;
import model.Cliente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Calendar;

@SuppressWarnings("serial")
public class MantemCliente extends Funcionalidade {
	
	private static MantemCliente mantemCliente = null;
	private JTextField textFieldNome;
	private JTextField textFieldEndereco;
	
	private JLabel lblNo;
	private JLabel lblNascimento;
	
	private JButton btnSalvar;
	private JButton btnVerificarCPF;
	
	private JFormattedTextField fTextFieldCPF;
	private JFormattedTextField textFieldNascimento;
	private JFormattedTextField formattedTextFieldTelefone;
	private JButton btnLimpaFormulrio;

	
	private MantemCliente() {
		super("Manter Cliente");
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setBounds(10, 97, 107, 14);
		this.add(lblNewLabel_1);
		
		textFieldNome = new JTextField();
		textFieldNome.setBounds(158, 94, 266, 20);
		textFieldNome.setColumns(10);
		this.add(textFieldNome);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(10, 67, 107, 14);
		this.add(lblCpf);
		
		fTextFieldCPF = new JFormattedTextField(Mascara.mascaraCPF());
		fTextFieldCPF.setBounds(158, 63, 107, 20);
		fTextFieldCPF.setColumns(14);
		this.add(fTextFieldCPF);
		
		JLabel lblNewLabel_2 = new JLabel("Endere\u00E7o");
		lblNewLabel_2.setBounds(10, 125, 107, 14);
		this.add(lblNewLabel_2);
		
		textFieldEndereco = new JTextField();
		textFieldEndereco.setBounds(158, 126, 266, 20);
		textFieldEndereco.setColumns(10);
		this.add(textFieldEndereco);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(10, 156, 107, 14);
		this.add(lblTelefone);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salvarDadosCliente();
			}
		});
		btnSalvar.setBounds(361, 276, 89, 23);
		this.add(btnSalvar);
		
		btnVerificarCPF = new JButton("Verificar");
		btnVerificarCPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				verificaCPF();
			}
		});
		btnVerificarCPF.setBounds(292, 63, 89, 23);
		this.add(btnVerificarCPF);
		
		lblNascimento = new JLabel("Nascimento");
		lblNascimento.setBounds(10, 193, 107, 14);
		add(lblNascimento);
		
		textFieldNascimento = new JFormattedTextField(Mascara.mascaraData());
		textFieldNascimento.setBounds(158, 188, 107, 20);
		add(textFieldNascimento);
		textFieldNascimento.setColumns(10);
		
		JLabel lblListaNegra = new JLabel("Lista Negra");
		lblListaNegra.setBounds(10, 228, 107, 14);
		add(lblListaNegra);
		
		lblNo = new JLabel("N\u00E3o");
		lblNo.setBounds(158, 228, 46, 14);
		add(lblNo);
		
		formattedTextFieldTelefone = new JFormattedTextField(Mascara.mascaraTelefone());
		formattedTextFieldTelefone.setColumns(15);
		formattedTextFieldTelefone.setBounds(158, 157, 107, 20);
		add(formattedTextFieldTelefone);
		
		btnLimpaFormulrio = new JButton("Limpa Formul\u00E1rio");
		btnLimpaFormulrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				apagaFormulario();
			}
		});
		btnLimpaFormulrio.setBounds(80, 276, 139, 23);
		add(btnLimpaFormulrio);
	}

	public static MantemCliente getInstance(){
		if( mantemCliente == null ){
			mantemCliente = new MantemCliente();
		}
		
		return mantemCliente;
	}
	public void apagaFormulario(){

		this.fTextFieldCPF.setText("");
		this.textFieldNome.setText("");
		this.textFieldEndereco.setText("");
		this.formattedTextFieldTelefone.setText("");
		this.textFieldNascimento.setText("");
		this.lblNo.setText("No");
		
		this.fTextFieldCPF.setEditable(true);
		super.limpaMensagem();
	}
	
	public <T> void carregaFormulario(T t){
		Cliente cliente = (Cliente)t;
		
		this.fTextFieldCPF.setText(""+cliente.getCpf());
		this.fTextFieldCPF.setEditable(false);
		this.textFieldNome.setText(cliente.getNome());
		this.textFieldEndereco.setText(cliente.getEndereco());
		this.formattedTextFieldTelefone.setText(cliente.getTelefone());
		
		String mesStr = "" + (cliente.getNascimento().get(Calendar.MONTH)+1),
			   diaStr = "" + cliente.getNascimento().get(Calendar.DAY_OF_MONTH);
		
		if (cliente.getNascimento().get(Calendar.MONTH)+1 < 10)
			mesStr = "0".concat(mesStr);
		if (cliente.getNascimento().get(Calendar.DAY_OF_MONTH) < 10)
			diaStr = "0".concat(diaStr);
			
		this.textFieldNascimento.setText( diaStr +""+ mesStr + cliente.getNascimento().get(Calendar.YEAR));
	}
	
	public Cliente leFormulario(){
		long cpf = Util.StringToLong(this.fTextFieldCPF.getText().replaceAll("[_.-]", ""));
		
		Cliente c = Controle.getCliente(cpf);
		long id = 0;
		if( c != null ){
			id = c.getId();
		}
		Cliente cliente = new Cliente(	id,
										cpf,
										this.textFieldNome.getText(),
										this.textFieldEndereco.getText(),
										this.formattedTextFieldTelefone.getText(),
										Util.stringToCalendar(this.textFieldNascimento.getText()) );
		
		return cliente;
	}
	
	public void verificaCPF(){
		
		long cpf = Util.StringToLong(this.fTextFieldCPF.getText().replaceAll("[_.-]", ""));
		Cliente cliente = Controle.getCliente(cpf);
		if(cliente == null ){
			apagaFormulario();
			super.setMensagemErro("Nenhum cliente encontrado!");
			
			this.fTextFieldCPF.setText(""+cpf);
			return;
		}
		
		carregaFormulario(cliente);
		
		/*if( cliente.isListaNegra() )
			this.lblNo.setText("Sim");
		else{
			this.lblNo.setText("No");
		}*/
		
		super.setMensagemConclusao("Dados do cliente carregados!");
	}
	
	public void salvarDadosCliente(){
		
		Cliente cliente = leFormulario();
		
		if( Controle.salvarCliente(cliente) ){
			super.setMensagemConclusao("Os dados foram atualizados!");
		}
		else{
			super.setMensagemErro("Erro ao salvar dados!");
		}
	}
}
