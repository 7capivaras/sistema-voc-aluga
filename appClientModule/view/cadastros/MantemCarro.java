package view.cadastros;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

import view.Funcionalidade;
import view.Login;
import view.Mascara;
import control.Controle;
import control.DAO.AgenciaDAO;
import model.Funcionario;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import model.Agencia;
import model.Carro;
import model.Util;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class MantemCarro extends Funcionalidade{

	private static MantemCarro mantemCarro = null;
	private JTextField modeloTextField;
	private JTextField marcaTextField;
	private JTextArea descricaoTextArea;
	
	private JButton btnSalvar;
	private JButton btnVerificar;
	
	private JFormattedTextField formattedTextFieldChassi;
	private JFormattedTextField formattedTextFieldPlaca;
	private JFormattedTextField formattedTextFieldDataAquisicao;
	private JFormattedTextField formattedTextFieldUltimaRevisao;
	private JFormattedTextField formattedTextFieldValorDiario;
	private JFormattedTextField formattedTextFieldKmRodados;
	
	private Funcionario funcionario;

	private MantemCarro() {
		super("Manter Carro");
		super.setPermissaoAcesso(new String[]{Funcionalidade.GERENTE_PERMITIDO});
		
		initLabels();
		initTextFields();
		initButtons();
		initFormattedTextFields();
	}
	
	public static MantemCarro getInstance(){
		if(mantemCarro == null){
			mantemCarro = new MantemCarro();
		}
		
		return mantemCarro;
	}
	
	private void initLabels(){
		
		JLabel lblChassi = new JLabel("Chassi");
		lblChassi.setBounds(10, 67, 102, 14);
		this.add(lblChassi);
		
		JLabel lblPlaca = new JLabel("Placa");
		lblPlaca.setBounds(266, 88, 67, 14);
		this.add(lblPlaca);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(10, 101, 102, 14);
		this.add(lblModelo);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(266, 119, 67, 14);
		this.add(lblMarca);
		
		JLabel lblDataAquisicao = new JLabel("Data de Aquisi��o");
		lblDataAquisicao.setBounds(10, 131, 102, 17);
		this.add(lblDataAquisicao);
		
		JLabel lblDescricao = new JLabel("Descri��o");
		lblDescricao.setBounds(266, 161, 67, 17);
		this.add(lblDescricao);
		
		JLabel lblKmRodados = new JLabel("Km Rodados");
		lblKmRodados.setBounds(10, 191, 102, 20);
		this.add(lblKmRodados);
	
		JLabel lblUltimaRevisao = new JLabel("�ltima Revis�o");
		lblUltimaRevisao.setBounds(10, 159, 102, 21);
		this.add(lblUltimaRevisao);
		
		JLabel lblValorDiario = new JLabel("Valor Di�rio");
		lblValorDiario.setBounds(10, 223, 102, 14);
		this.add(lblValorDiario);
		
		
	}
	
	private void initTextFields(){
		modeloTextField = new JTextField();
		modeloTextField.setBounds(122, 98, 119, 20);
		this.add(modeloTextField);
		modeloTextField.setColumns(10);

		marcaTextField = new JTextField();
		marcaTextField.setBounds(343, 116, 119, 20);
		this.add(marcaTextField);
		marcaTextField.setColumns(10);
		
		descricaoTextArea = new JTextArea();
		descricaoTextArea.setBounds(343, 157, 119, 79);
		this.add(descricaoTextArea);
		descricaoTextArea.setColumns(10);
	}
	
	private void initButtons(){
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salvarCarro();
			}
		});
		btnSalvar.setBounds(373, 293, 89, 23);
		this.add(btnSalvar);
		
		btnVerificar = new JButton("Verificar Placa");
		btnVerificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				verificaPlaca();
			}
		});
		btnVerificar.setBounds(213, 293, 120, 23);
		this.add(btnVerificar);
	}
	
	private void initFormattedTextFields(){
		formattedTextFieldChassi = new JFormattedTextField(Mascara.mascaraChassi());
		formattedTextFieldChassi.setColumns(17);
		formattedTextFieldChassi.setBounds(122, 64, 136, 20);
		add(formattedTextFieldChassi);
		
		formattedTextFieldPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		formattedTextFieldPlaca.setColumns(8);
		formattedTextFieldPlaca.setBounds(343, 85, 119, 20);
		add(formattedTextFieldPlaca);
		
		formattedTextFieldDataAquisicao = new JFormattedTextField(Mascara.mascaraData());
		formattedTextFieldDataAquisicao.setColumns(10);
		formattedTextFieldDataAquisicao.setBounds(122, 129, 119, 20);
		add(formattedTextFieldDataAquisicao);
		
		formattedTextFieldUltimaRevisao = new JFormattedTextField(Mascara.mascaraData());
		formattedTextFieldUltimaRevisao.setColumns(10);
		formattedTextFieldUltimaRevisao.setBounds(122, 159, 119, 20);
		add(formattedTextFieldUltimaRevisao);
		
		formattedTextFieldValorDiario = new JFormattedTextField();
		formattedTextFieldValorDiario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				formatadorDecimal(formattedTextFieldValorDiario);
			}
		});
		formattedTextFieldValorDiario.setColumns(10);
		formattedTextFieldValorDiario.setBounds(122, 222, 119, 20);
		add(formattedTextFieldValorDiario);
		
		formattedTextFieldKmRodados = new JFormattedTextField();
		formattedTextFieldKmRodados.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				formatadorDecimal(formattedTextFieldKmRodados);
			}
		});
		formattedTextFieldKmRodados.setColumns(10);
		formattedTextFieldKmRodados.setBounds(122, 191, 119, 20);
		add(formattedTextFieldKmRodados);
		
		JButton btnLimpaFormulario = new JButton("Limpa Formul\u00E1rio");
		btnLimpaFormulario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				apagaFormulario();
			}
		});
		btnLimpaFormulario.setBounds(20, 293, 136, 23);
		add(btnLimpaFormulario);
	}
	
	private void formatadorDecimal( JFormattedTextField ftf ){
		String decimalString = ftf.getText().replaceAll("[^0-9\\.]", "");
		ftf.setText(decimalString);
	}
	
	public void apagaFormulario(){
		
		this.modeloTextField.setText("");
		this.marcaTextField.setText("");
		this.descricaoTextArea.setText("");
		
		this.formattedTextFieldChassi.setText("");
		this.formattedTextFieldPlaca.setText("");
		this.formattedTextFieldDataAquisicao.setText("");
		this.formattedTextFieldUltimaRevisao.setText("");
		this.formattedTextFieldValorDiario.setText("");
		this.formattedTextFieldKmRodados.setText("");
		
		this.formattedTextFieldChassi.setEditable(true);
		super.limpaMensagem();
	}
	
	
	public <T> void carregaFormulario(T t){
		Carro carro = (Carro)t;
		this.modeloTextField.setText(carro.getModelo());
		this.marcaTextField.setText(carro.getMarca());
		this.descricaoTextArea.setText(carro.getDescricao());
		
		this.formattedTextFieldChassi.setText(carro.getChassi());
		this.formattedTextFieldChassi.setEditable(false);
		
		this.formattedTextFieldPlaca.setText(carro.getPlaca());
		
		this.formattedTextFieldDataAquisicao.setText(Util.calendarToString(carro.getDataAquisicao()));
		this.formattedTextFieldUltimaRevisao.setText(Util.calendarToString(carro.getUltimaRevisao()));
		
		this.formattedTextFieldValorDiario.setText(carro.getValorDiaria()+"");
		this.formattedTextFieldKmRodados.setText(carro.getKmRodados()+"");
	}
	
	
	public Carro leFormulario(){

		this.funcionario = Login.getInstance().getFuncionario();
		
		String chassi = this.formattedTextFieldChassi.getText();
		Carro c = Controle.getCarroByChassi(chassi);
		long id = 0;
		Agencia agencia = AgenciaDAO.getAgenciaById(this.funcionario.getAgencia().getIdAgencia());
		
		if( c != null){
			id = c.getId();
			agencia = c.getAgencia();
		}
		
		Carro carro = new Carro(	id, 
									this.formattedTextFieldChassi.getText(),
									this.marcaTextField.getText(),
									this.modeloTextField.getText(),
									this.descricaoTextArea.getText(),
									Util.stringToCalendar(this.formattedTextFieldDataAquisicao.getText()),
									Util.stringToDouble(this.formattedTextFieldKmRodados.getText()),
									Util.stringToCalendar(this.formattedTextFieldUltimaRevisao.getText()),
									this.formattedTextFieldPlaca.getText(),
									Util.stringToDouble(this.formattedTextFieldValorDiario.getText()));
		
		carro.setAgencia(agencia);
		return carro;
	}
	
	public void verificaPlaca(){
		String placa = this.formattedTextFieldPlaca.getText();
		Carro carro = Controle.getCarro(placa);
		
		if(carro == null){
			this.apagaFormulario();
			super.setMensagemErro("Nenhum carro encontrado!");
			
			this.formattedTextFieldPlaca.setText(placa);
			return;
		}
		
		this.carregaFormulario(carro);
		
		super.setMensagemConclusao("Dados carregados!");
	}
	
	public void salvarCarro(){
		Carro carro = this.leFormulario();
		
		if(Controle.salvarCarro(carro)){
			super.setMensagemConclusao("Dados salvos!");
		}
		else{
			super.setMensagemErro("Erro ao salvar dados!");
		}
	}
}
