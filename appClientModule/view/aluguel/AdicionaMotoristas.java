package view.aluguel;

import model.Motorista;
import model.Util;
import view.Lista;
import view.Mascara;
import view.Principal;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JFormattedTextField;

@SuppressWarnings("serial")
public class AdicionaMotoristas extends Lista<Motorista> {
	
	private static AdicionaMotoristas adicionaMotoristas = null;
	
	private JTextField textFieldCnh;
	private JTextField textFieldNome;
	private JFormattedTextField jFTextFieldCpf;

	private AdicionaMotoristas() {
		super("Lista de Motoristas");
		super.setSelecaoSigular();
		super.setTamanhoLista(100);
		
		String[] cabecalho = new String[]{"CPF","CNH","Nome"};
		super.setCabecalho(cabecalho);
		
		this.initLabels();
		this.initTextFields();
		this.initButtons();

	}
	
	public void carregaDados(ArrayList<Motorista> ms){
		super.carregaDados(ms);
	}
	
	public static AdicionaMotoristas getInstance(){
		if( adicionaMotoristas == null ){
			adicionaMotoristas = new AdicionaMotoristas();
		}
		
		return adicionaMotoristas;
	}
	
	
	private void initLabels(){
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(43, 194, 34, 14);
		add(lblCpf);
		
		JLabel lblCnh = new JLabel("CNH:");
		lblCnh.setBounds(183, 195, 34, 14);
		add(lblCnh);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(312, 194, 46, 14);
		add(lblNome);
	}

	private void initTextFields(){
		textFieldCnh = new JTextField();
		textFieldCnh.setBounds(222, 191, 86, 20);
		add(textFieldCnh);
		textFieldCnh.setColumns(10);
		
		textFieldNome = new JTextField();
		textFieldNome.setBounds(360, 191, 86, 20);
		add(textFieldNome);
		textFieldNome.setColumns(10);

		jFTextFieldCpf = new JFormattedTextField(Mascara.mascaraCPF());
		jFTextFieldCpf.setBounds(78, 191, 96, 20);
		add(jFTextFieldCpf);
	}
	
	private void initButtons(){
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adicionar();
			}
		});
		btnAdicionar.setBounds(359, 219, 89, 23);
		add(btnAdicionar);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editar();
			}
		});
		btnEditar.setBounds(260, 219, 89, 23);
		add(btnEditar);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				excluir();
			}
		});
		btnExcluir.setBounds(161, 219, 89, 23);
		add(btnExcluir);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salvar();
			}
		});
		btnSalvar.setBounds(359, 268, 89, 23);
		add(btnSalvar);
	}
	
	protected void salvar() {
		Principal.getInstance().trocaPanel(AlugarCarro.getInstance());
		AlugarCarro.getInstance().carregaMotoristas(super.getListaT());
	}

	protected void excluir() {
		Motorista m = super.getDadoSelecionado();
		if(m == null){
			super.setMensagemAlerta("Nenhum motorista selecionado!");
			return;
		}
		
		super.removeDadoSelecionado();
		super.setMensagemConclusao("Dado exclu�do da lista!");
	}

	protected void editar() {
		Motorista m = super.getDadoSelecionado();
		if(m == null){
			super.setMensagemAlerta("Nenhum motorista selecionado!");
			return;
		}
		super.removeDadoSelecionado();
		this.textFieldCnh.setText(m.getCnh());
		this.textFieldNome.setText(m.getNome());
		this.jFTextFieldCpf.setText(m.getCpf()+"");
		super.setMensagemConclusao("Dado carregado para edi��o!");
	}

	protected void adicionar() {
		if( this.jFTextFieldCpf.getText().replaceAll("[_.-]", "").equals("") 
				|| this.textFieldCnh.getText().equals("")
				|| this.textFieldNome.getText().equals("") ){
			super.setMensagemAlerta("Dados inv�lidos ou n�o informados!");
			return;
		}
		
		long cpf = Util.StringToLong(this.jFTextFieldCpf.getText().replaceAll("[_.-]", ""));
		
		Motorista m = new Motorista(	0,
										cpf,
										this.textFieldCnh.getText(),
										this.textFieldNome.getText() );
		super.adicionaDado(m);
		super.setMensagemConclusao("Dado adicionado com sucesso!");
	}

	@Override
	public void apagaFormulario() {
		super.apagaFormulario();
		this.textFieldCnh.setText("");
		this.textFieldNome.setText("");
		this.jFTextFieldCpf.setText("");
		super.limpaMensagem();
	}

	@Override
	protected String[] getTupla(Motorista m) {
		String[] tupla = new String[]{ m.getCpf()+"", m.getCnh(), m.getNome()};
		return tupla;
	}
}
