package view.consultas;

import java.util.ArrayList;

import control.Controle;
import model.Locacao;
import model.Util;

import javax.swing.JButton;

import view.Lista;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaLocacoes extends Lista<Locacao>{
	
	private static ListaLocacoes listaLocacoes = null;
	private ArrayList<Locacao> locacoes;
	
	public ListaLocacoes() {
		super("Lista de Loca��es");
		String[] cabecalho = new String[]{	"Nome Agencia",
											"Nome Cliente", 
											"Data Loca��o", 
											"Data Retirada", 
											"Previs�o Entrega"};
		super.setCabecalho(cabecalho);
		super.setSelecaoSigular();
		
		JButton btnCarregar = new JButton("Carregar");
		btnCarregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDados();
			}
		});
		btnCarregar.setBounds(229, 291, 89, 23);
		add(btnCarregar);
		
	}
	
	public static ListaLocacoes getInstance(){
		if(listaLocacoes == null){
			listaLocacoes = new ListaLocacoes();
		}
		
		return listaLocacoes;
	}

	protected void setDados() {
		this.locacoes = Controle.getLocacoes();
		super.carregaDados(locacoes);
	}
	
	protected String[] getTupla(Locacao l){
		String[] tupla = new String[]{ 	l.getAgencia().getNome()+"", 
										l.getCliente().getNome()+"", 
										Util.calendarToString(l.getDataLocacao())+"",  
										Util.calendarToString(l.getDataRetirada())+"",
										Util.calendarToString(l.getPrevisaoEntrega())+""};
		return tupla;
	}
	
}