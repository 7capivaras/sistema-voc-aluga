package view.pagamento;

import java.awt.Font;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Principal;
import model.Pagamento;
import model.Venda;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ExibirNota extends Funcionalidade {

	private static ExibirNota exibirNota = null;
	
	private JLabel taxas, total, subtotal, descricao, lblCpf, lblNome, lblMarca, lblModelo;
	private Funcionalidade funcionalidade; 
	
	private ExibirNota() {
		super("Nota Fiscal");
		
		this.funcionalidade = null;
		
		JLabel label = new JLabel("Prestador de Servi\u00E7os");
		label.setFont(new Font("Dialog", Font.BOLD, 12));
		label.setBounds(10, 50, 141, 22);
		add(label);
		
		JLabel label_3 = new JLabel("Tomador de Servi\u00E7os");
		label_3.setFont(new Font("Dialog", Font.BOLD, 12));
		label_3.setBounds(10, 134, 124, 22);
		add(label_3);
		
		lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(37, 162, 168, 22);
		add(lblCpf);
		
		JLabel lblCNPJRazaoSocial = new JLabel("CNPJ: 04.944.973/0001-30             Nome/Raz\u00E3o social: Voc\u00EA Aluga");
		lblCNPJRazaoSocial.setBounds(37, 78, 382, 22);
		add(lblCNPJRazaoSocial);
		
		lblNome = new JLabel("New label");
		lblNome.setBounds(214, 162, 260, 22);
		add(lblNome);
		
		JLabel label_8 = new JLabel("Descri\u00E7\u00E3o Servi\u00E7os");
		label_8.setFont(new Font("Dialog", Font.BOLD, 12));
		label_8.setBounds(10, 195, 124, 22);
		add(label_8);
		
		descricao = new JLabel("Aluguel de carros");
		descricao.setBounds(23, 228, 209, 14);
		add(descricao);
		
		JLabel lblTaxas = new JLabel("Taxas:");
		lblTaxas.setBounds(308, 211, 62, 14);
		add(lblTaxas);
		
		taxas = new JLabel("R$ 0");
		taxas.setBounds(376, 211, 82, 14);
		add(taxas);
		
		JLabel lblTotal = new JLabel("Total:");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTotal.setBounds(308, 259, 62, 14);
		add(lblTotal);
		
		total = new JLabel("R$ 0");
		total.setBounds(376, 259, 82, 14);
		add(total);
		
		JButton btnNewButton_1 = new JButton("Imprimir");
		btnNewButton_1.setBounds(378, 293, 96, 23);
		add(btnNewButton_1);
		
		JLabel lblSubtotal = new JLabel("Subtotal:");
		lblSubtotal.setBounds(308, 239, 62, 14);
		add(lblSubtotal);
		
		subtotal = new JLabel("R$ 0");
		subtotal.setBounds(376, 239, 82, 14);
		add(subtotal);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				retornaTelaAnterior();
			}
		});
		btnVoltar.setBounds(266, 293, 89, 23);
		add(btnVoltar);
		
		lblMarca = new JLabel("Marca: ");
		lblMarca.setBounds(23, 253, 209, 14);
		add(lblMarca);
		
		lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(23, 278, 209, 14);
		add(lblModelo);
		
	}

	public static ExibirNota getInstance() {
		if( exibirNota == null ){
			exibirNota = new ExibirNota();
		}
		
		return exibirNota;
	}
	
	public void retornaTelaAnterior(){
		Principal.getInstance().trocaPanel(this.funcionalidade);
	}
	
	public <T> void carregaFormulario(T t){
		long cpf = 0;
		String nome = "";
		String marca = "";
		String modelo = "";
		String descricao = "";
		double total = 0;
		double subtotal = 0;
		double taxas = 0;
		
		if(t instanceof Pagamento){
			Pagamento p = (Pagamento)t;
			cpf = p.getLocacao().getCliente().getCpf();
			nome = p.getLocacao().getCliente().getNome();
			marca = p.getLocacao().getCarro().getMarca();
			modelo = p.getLocacao().getCarro().getModelo();
			descricao = "Aluguel de carro";
			taxas = p.totalDeTaxas();
			subtotal = p.getValor();
			total = p.valorTotal();
		}
		else if (t instanceof Venda){
			Venda v = (Venda)t;
			cpf = v.getCliente().getCpf();
			nome = v.getCliente().getNome();
			marca = v.getCarro().getMarca();
			modelo = v.getCarro().getModelo();
			descricao = "Venda de carro";
			total = v.getValor();
		}
		else
			return;
		
		this.lblCpf.setText("CPF:  "+cpf);
		this.lblNome.setText("Nome:  "+nome);
		this.descricao.setText(descricao);
		this.lblMarca.setText("Marca: "+marca);
		this.lblModelo.setText("Modelo: "+modelo);
		this.taxas.setText("R$ "+taxas);
		this.subtotal.setText("R$ "+subtotal);
		this.total.setText("R$ "+total);
	}
	
	public <T> void carregaNotaFiscal(Funcionalidade f, T t){
		this.funcionalidade = f;
		Principal.getInstance().trocaPanel(this);
		carregaFormulario(t);
	}

	@Override
	public void apagaFormulario() {
		this.lblCpf.setText("CPF:  ");
		this.lblNome.setText("Nome:  ");
		this.descricao.setText("");
		this.lblMarca.setText("Marca: ");
		this.lblModelo.setText("Modelo: ");
		this.taxas.setText("R$ 0");
		this.subtotal.setText("R$ 0");
		this.total.setText("R$ 0");
	}
}
