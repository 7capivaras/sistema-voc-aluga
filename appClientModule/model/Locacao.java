package model;
import java.util.ArrayList;
import java.util.Calendar;

public class Locacao
{
	private long idLocacao;
	private Calendar dataLocacao;
	private Calendar previsaoEntrega;
	private Calendar dataEntrega;
	private Calendar dataRetirada;
	
	private Cliente cliente;
	private Agencia agencia;
	private CartaoCredito cartaoCredito;
	private Carro carro;
	
	private ArrayList<Pagamento> pagamento;
	private ArrayList<Motorista> motoristas;

	public Locacao( long id ){
		this.idLocacao = id;
	}
	
	public Locacao(	long id,
					Calendar dataLocacao,
					Calendar dataEntrega,
					Calendar previsaoEntrega,
					Calendar dataRetirada )
	{
		this.idLocacao = id;
		this.dataLocacao = dataLocacao;
		this.dataEntrega = dataEntrega;
		this.previsaoEntrega = previsaoEntrega;
		this.dataRetirada = dataRetirada;
		
		this.cliente = null;
		this.agencia = null;
		this.cartaoCredito = null;
		this.carro = null;
		
		this.pagamento = new ArrayList<Pagamento>();
		this.motoristas = new ArrayList<Motorista>();
	}
	
	public void addPagamento(Pagamento p){
		p.setLocacao(this);
		this.pagamento.add(p);
	}
	
	public void addMotorista(Motorista m){
		m.setLocacao(this);
		this.motoristas.add(m);
	}
	
	public ArrayList<Pagamento> getPagamento() {
		return pagamento;
	}

	public ArrayList<Motorista> getMotoristas() {
		return motoristas;
	}

	public int tempoDeAluguel(){
		int dias = 0;
		long tempoL = this.previsaoEntrega.getTimeInMillis() - this.dataRetirada.getTimeInMillis();
		Calendar tempoCAL = Calendar.getInstance();
		tempoCAL.setTimeInMillis(tempoL);
		dias = tempoCAL.get(Calendar.DAY_OF_YEAR);
		return dias;
	}
	
	public int tempoDeAtraso(){
		int dias = 0;
		if(this.haTaxaAtraso()){
			long tempoL = Calendar.getInstance().getTimeInMillis() - this.previsaoEntrega.getTimeInMillis();
			Calendar tempoCAL = Calendar.getInstance();
			tempoCAL.setTimeInMillis(tempoL);
			dias = tempoCAL.get(Calendar.DAY_OF_YEAR);
		}
		return dias;
	}
	
	public int tempoDeAdiantamento(){
		int dias = 0;
		if(this.haDescontoDeAdiantamento()){
			long tempoL = this.previsaoEntrega.getTimeInMillis() - this.dataEntrega.getTimeInMillis();
			Calendar tempoCAL = Calendar.getInstance();
			tempoCAL.setTimeInMillis(tempoL);
			dias = tempoCAL.get(Calendar.DAY_OF_YEAR);
		}
		return dias;
	}
	
 	public boolean haTaxaAtraso(){
		Boolean ha = false;
		
		if( this.dataEntrega != null){
			if(this.dataEntrega.equals(this.previsaoEntrega)) ha = false;
			else {
				if( this.dataEntrega.after(this.previsaoEntrega) ) ha = true;
				else ha = false;
			}
		}
		else{
			if(Calendar.getInstance().equals(this.previsaoEntrega)) ha = false;
			else {
				if(Calendar.getInstance().before(this.previsaoEntrega)) ha = false;
				else ha = true;
			}
		}
		
		return ha;
	}
	
	public boolean haTaxaDanificacao(){
		boolean ha = false;

		if( this.dataEntrega != null) ha = true;
		else ha = false;
		
		return ha;
	}
	
	public boolean haTaxaRetorno(Agencia agenciaDeRetorno){
		boolean ha = false;
		if( this.dataEntrega != null){
			if(agenciaDeRetorno.getIdAgencia() == this.carro.getAgencia().getIdAgencia() ) ha = false;
			else ha = true;
		}
		else ha = false;

		return ha;
	}
	
	public boolean haDescontoDeAdiantamento(){
		boolean ha = false;
		
		if( this.dataEntrega != null){
			if( this.dataEntrega.equals(this.previsaoEntrega)) ha = false;
			else{ 
				if( this.dataEntrega.after(this.previsaoEntrega) ) ha = false;
				else ha = true;
			}
		}
		else ha = false;
		
		return ha;
	}
	
	public void setDataEntregaHoje() {
		this.dataEntrega = Calendar.getInstance();
	}

	public Calendar getPrevisaoEntrega() {
		return previsaoEntrega;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public CartaoCredito getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCredito cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public long getIdLocacao() {
		return idLocacao;
	}

	public Calendar getDataLocacao() {
		return dataLocacao;
	}

	public Calendar getDataEntrega() {
		return dataEntrega;
	}

	public Calendar getDataRetirada() {
		return dataRetirada;
	}
	
}

