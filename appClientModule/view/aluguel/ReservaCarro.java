package view.aluguel;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import view.Funcionalidade;
import view.Login;
import view.Mascara;
import control.Controle;
import model.Carro;
import model.Funcionario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ReservaCarro extends Funcionalidade {
	
	private static ReservaCarro reservaCarro = null;
	private JFormattedTextField jFTextPlaca;
	private JFormattedTextField jFTextDataRetirada;
	private JFormattedTextField jFTextDataDevolucao;
	private JFormattedTextField formattedTextFieldCPF;
	
	private JButton btnVerificarCPF;
	private JButton btnVerificarCarro;
	private JButton btnReservar;
	private JButton btnEscolherCarro;
	
	private Funcionario funcionario;
	private Carro carro;

	private ReservaCarro() {
		super("Reserva de Carro");
		
		JLabel lblCpfDoCliente = new JLabel("CPF");
		lblCpfDoCliente.setBounds(10, 82, 31, 14);
		this.add(lblCpfDoCliente);
		
		btnVerificarCPF = new JButton("Verificar");
		btnVerificarCPF.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				verificarCPF();
			}
		});
		btnVerificarCPF.setBounds(319, 78, 155, 23);
		this.add(btnVerificarCPF);
		
		JLabel lblPlacaDoCarro = new JLabel("Placa do Carro");
		lblPlacaDoCarro.setBounds(10, 118, 86, 14);
		this.add(lblPlacaDoCarro);
		
		jFTextPlaca = new JFormattedTextField(Mascara.mascaraPlaca());
		jFTextPlaca.setToolTipText("Digite a placa no seguite formato: PLC-1234");
		jFTextPlaca.setBounds(152, 115, 119, 20);
		this.add(jFTextPlaca);
		jFTextPlaca.setColumns(10);
		
		JLabel lblDataRetirada = new JLabel("Data da Retirada");
		lblDataRetirada.setBounds(10, 159, 132, 14);
		this.add(lblDataRetirada);
	 
		jFTextDataRetirada = new JFormattedTextField(Mascara.mascaraData());
		jFTextDataRetirada.setBounds(152, 156, 119, 20);
		jFTextDataRetirada.setToolTipText("Digite a data no seguinte formato: dd/mm/aaaa");
		this.add(jFTextDataRetirada);
		
		JLabel lblDataDevolucao = new JLabel("Data da Devolu��o");
		lblDataDevolucao.setBounds(10, 200, 132, 14);
		this.add(lblDataDevolucao);
	 
		jFTextDataDevolucao = new JFormattedTextField(Mascara.mascaraData());
		jFTextDataDevolucao.setBounds(152, 195, 119, 20);
		jFTextDataDevolucao.setToolTipText("Digite a data no seguinte formato: dd/mm/aaaa");
		this.add(jFTextDataDevolucao);
		
		btnVerificarCarro = new JButton("Ver Disponibilidade");
		btnVerificarCarro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				verificaDisponibilidadeCarro();
			}
		});
		btnVerificarCarro.setBounds(319, 195, 155, 23);
		this.add(btnVerificarCarro);
				
		btnReservar = new JButton("Reservar");
		btnReservar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				fazerReserva();
			}
		});
		btnReservar.setBounds(385, 278, 89, 23);
		this.add(btnReservar);
		
		btnEscolherCarro = new JButton("Escolher Carro");
		btnEscolherCarro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SelecionaCarro.getInstance().escolheCarro(ReservaCarro.getInstance());
			}
		});
		btnEscolherCarro.setBounds(319, 114, 155, 23);
		this.add(btnEscolherCarro);
		
		formattedTextFieldCPF = new JFormattedTextField(Mascara.mascaraCPF());
		formattedTextFieldCPF.setColumns(14);
		formattedTextFieldCPF.setBounds(152, 79, 119, 20);
		add(formattedTextFieldCPF);
		
	}
	
	public static ReservaCarro getInstance(){
		if( reservaCarro == null ){
			reservaCarro = new ReservaCarro();
		}
		
		return reservaCarro;
	}
	private void verificarCPF() {
		
		if( Controle.verificarClienteCPF(this.formattedTextFieldCPF.getText().replaceAll("[_.-]", "") ) ){
			super.limpaMensagem();
		}else{
			super.setMensagemErro("Cliente n�o encontrado!");
		}
		
	}
	
	private void verificaDisponibilidadeCarro() {
		
		if( Controle.verificaDisponibilidadeCarro(this.jFTextPlaca.getText(), this.jFTextDataRetirada.getText(), this.jFTextDataDevolucao.getText())){
			super.setMensagemConclusao("Carro dispon�vel");
		}else{
			super.setMensagemErro("Carro n�o existente ou indispon�vel!");
		}		
	}
	
	private void fazerReserva() {
		
		this.funcionario = Login.getInstance().getFuncionario();
		
		if( Controle.reservarCarro(this.formattedTextFieldCPF.getText().replaceAll("[_.-]", ""), 
				this.jFTextPlaca.getText(), this.jFTextDataRetirada.getText(),
				this.jFTextDataDevolucao.getText(), this.funcionario.getId() ) ){
			
			super.setMensagemConclusao("Reserva efetuada com sucesso");
			
		}else{
			super.setMensagemErro("Erro na reserva");
		}
	}
	
	public void apagaFormulario(){
		this.formattedTextFieldCPF.setText("");
		this.jFTextPlaca.setText("");
		this.jFTextDataRetirada.setText("");
		this.jFTextDataDevolucao.setText("");
		super.limpaMensagem();
	}

	public <T> void carregaFormulario(T t) {
		Carro carro = (Carro)t;
		this.carro = carro;
		this.jFTextPlaca.setText(this.carro.getPlaca());
	}

}
