package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import control.ConnectionFactory;
import model.Cliente;
import model.Util;

public class ClienteDAO {
	
	public boolean salvarCliente( Cliente cliente ){
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			
			if( cliente.getId() == 0 ){
				stmt = con.prepareStatement("INSERT INTO voce_aluga.cliente( "
						+ "cpf, endereco, nascimento, nome, telefone) "
						+ "VALUES (?, ?, ?, ?, ?);");
			}
			else{
				stmt = con.prepareStatement("UPDATE voce_aluga.cliente "
						+ "SET cpf=?, endereco=?, nascimento=?, nome=?, telefone=? "
						+ "WHERE id=?;");
				stmt.setLong(6, cliente.getId());
			}
			
			stmt.setLong(1, cliente.getCpf());
			stmt.setString(2, cliente.getEndereco());
			stmt.setDate(3, Util.calendarToSQL(cliente.getNascimento()));
			stmt.setString(4, cliente.getNome());
			stmt.setString(5, cliente.getTelefone());
			
			stmt.execute();

			con.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean verificaCPF(long cpf) {
		
		boolean verificado = false;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Cliente where cpf = ?");
			stmt.setLong(1, cpf);

			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				verificado = true;
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return verificado;
	}

	public static Cliente getCliente(long cpf){
		
		Cliente cliente = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Cliente where cpf = ?");
			stmt.setLong(1, cpf);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				
				cliente = getCliente(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cliente;
		
	}

	public ArrayList<Cliente> getClientes() {
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		Cliente cliente = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Cliente;");
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				cliente = getCliente(resultSet);
				clientes.add(cliente);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(clientes.size() == 0){
			return null;
		}
		else
			return clientes;
	}

	public static Cliente getClienteById(long id) {
		Cliente cliente = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Cliente where id = ?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				
				cliente = getCliente(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cliente;
	}
	
	private static Cliente getCliente(ResultSet resultSet){
		
		Cliente cliente = null;
		
		try {
			cliente = new Cliente(	resultSet.getLong("id"),
									resultSet.getLong("cpf"),
									resultSet.getString("nome"),
									resultSet.getString("endereco"),
									resultSet.getString("telefone"),
									Util.stringSQLToCalendar(resultSet.getString("nascimento")) );
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cliente;
	}
}
