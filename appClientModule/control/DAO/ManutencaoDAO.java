package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import control.ConnectionFactory;
import model.Manutencao;
import model.Util;

public class ManutencaoDAO {

	public static boolean agendarManutencao(long idCarro, Calendar dataManutencao,
			String tipoServico, String motivo, long idFuncionario) {

		java.sql.Date data = Util.calendarToSQL(dataManutencao);
		
		boolean concluido = false;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("insert into voce_aluga.Manutencao(dataagendada, tiposervico, motivomanutencao, carro_id, funcionario_id)"
					+ " values (?, ?, ?, ?, ?)");
			
			stmt.setDate(1, data);
			stmt.setString(2, tipoServico);
			stmt.setString(3, motivo);
			stmt.setLong(4, idCarro);
			stmt.setLong(5, idFuncionario);

			stmt.execute();
			stmt.close();
			concluido = true;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}
	
	
	public static boolean haManutencao (Calendar data0, Calendar data1, long idCarro){
		
		boolean haManutencao = false;
		java.sql.Date dataInicio = Util.calendarToSQL(data0);
		java.sql.Date dataFim = Util.calendarToSQL(data1);
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Manutencao where carro_id = ? and dataagendada BETWEEN ? and ?");
			stmt.setLong(1, idCarro);
			stmt.setDate(2, dataInicio);
			stmt.setDate(3, dataFim);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				haManutencao = true;
			}
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return haManutencao;
	}
	
	public static long idManutencaoEmAndamento (long idCarro){

		long idManutencao = 0;
		java.sql.Date dataAtual = Util.calendarToSQL(Calendar.getInstance());

		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select idmanutencao from voce_aluga.Manutencao where carro_id = ? and datadevolucao IS NULL and dataagendada <= ?");
			stmt.setLong(1, idCarro);
			stmt.setDate(2, dataAtual);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				idManutencao = resultSet.getLong("idmanutencao");
			}
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return idManutencao;
	}


	public static boolean finalizarManutencao(long idManutencao) {
		
		java.sql.Date data = Util.calendarToSQL(Calendar.getInstance());
		
		boolean concluido = false;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("update voce_aluga.Manutencao set datadevolucao = ? where idmanutencao = ?");
			
			stmt.setDate(1, data);
			stmt.setLong(2, idManutencao);

			stmt.executeUpdate();
			stmt.close();
			concluido = true;
		} catch (SQLException e) {
			
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}

	public ArrayList<Manutencao> getManutencoes(){
		ArrayList<Manutencao> manutencoes = new ArrayList<Manutencao>();
		Manutencao manutencao = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
						
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Manutencao");
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()){
				manutencao = getManutencao(resultSet);
				manutencoes.add(manutencao);
			}
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return manutencoes;
		
	}
	
	private static Manutencao getManutencao(ResultSet resultSet){
		
		Manutencao manutencao = null;
		
		try {
			manutencao = new Manutencao(	resultSet.getLong("idmanutencao"),
											Util.stringSQLToCalendar(resultSet.getString("dataagendada")),
											resultSet.getString("tiposervico"),
											resultSet.getString("motivomanutencao") );
			
			manutencao.setCarro(CarroDAO.getCarroById(resultSet.getLong("carro_id")));
			manutencao.setFuncionario(FuncionarioDAO.getFuncionarioById(resultSet.getLong("funcionario_id")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return manutencao;
	}
}
