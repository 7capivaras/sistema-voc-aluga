package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import control.ConnectionFactory;
import model.Funcionario;

public class FuncionarioDAO {	
	
	public Funcionario verificaLogin(long cpf, String senha){
		
		Funcionario funcionario = this.getFuncionario(cpf);
		if( funcionario != null ){
			if( !funcionario.verificaSenha(senha) ){
				funcionario = null;
			}
		}
		return funcionario;
	}
	
	public boolean salvarFuncionario(Funcionario funcionario){
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		boolean concluido = false;
		try{
			if( funcionario.getId() == 0 ){
				stmt = con.prepareStatement("INSERT INTO voce_aluga.funcionario( "
						+ "cargo, cpf, endereco, nome, senhasistema, telefone, agencia_id) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?);");
			}else{
				stmt = con.prepareStatement("UPDATE voce_aluga.funcionario "
						+ "SET cargo=?, cpf=?, endereco=?, nome=?, senhasistema=?, telefone=?, agencia_id=? "
						+ "WHERE id=?;");
				stmt.setLong(8, funcionario.getId());
			}
			
			stmt.setString(1, funcionario.getCargo());
			stmt.setLong(2, funcionario.getCPF());
			stmt.setString(3, funcionario.getEndereco());
			stmt.setString(4, funcionario.getNome());
			stmt.setString(5, funcionario.getSenhaSistema());
			stmt.setString(6, funcionario.getTelefone());
			stmt.setLong(7, funcionario.getAgencia().getIdAgencia());
			
			stmt.execute();
			
			concluido = true;
			
		}catch (SQLException e) {
			e.printStackTrace();
			concluido = false;
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return concluido;
	}
	
	public Funcionario getFuncionario(long cpf){
		Funcionario funcionario = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Funcionario where cpf = ?");
			stmt.setLong(1, cpf);
			ResultSet resultSet = stmt.executeQuery();
			
			if( resultSet.next() ){
				funcionario = getFuncionario(resultSet);
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return funcionario;
	}

	public ArrayList<Funcionario> getFuncionarios() {
		ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();
		Funcionario funcionario = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Funcionario;");
			ResultSet resultSet = stmt.executeQuery();
			
			while( resultSet.next() ){
				funcionario = getFuncionario(resultSet);
				funcionarios.add(funcionario);
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(funcionarios.size() == 0){
			return null;
		}
		else
			return funcionarios;
	}
	
	private static Funcionario getFuncionario(ResultSet resultSet){
		
		Funcionario funcionario = null;
		
		try {
			funcionario = new Funcionario(	resultSet.getLong("id"),
											resultSet.getLong("cpf"),
											resultSet.getString("nome"),
											resultSet.getString("endereco"),
											resultSet.getString("telefone"),
											resultSet.getString("cargo"),
											resultSet.getString("senhasistema") );
			
			funcionario.setAgencia(AgenciaDAO.getAgenciaById(resultSet.getLong("agencia_id")));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return funcionario;
	}

	public static Funcionario getFuncionarioById(long id) {
		Funcionario funcionario = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Funcionario where id = ?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();
			
			if( resultSet.next() ){
				funcionario = getFuncionario(resultSet);
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return funcionario;
	}
}
