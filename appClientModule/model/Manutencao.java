package model;
import java.util.Calendar;

public class Manutencao
{
	private long idManutencao;
	private Calendar dataAgendada;
	private String tipoServico;
	private String motivoManutencao;

	private Carro carro;
	private Funcionario funcionario;

	public Manutencao(	long id,
						Calendar dataAgendada,
						String tipoServico,
						String motivoManutencao )
	{
		this.idManutencao = id;
		this.dataAgendada = dataAgendada;
		this.tipoServico = tipoServico;
		this.motivoManutencao = motivoManutencao;
		
		this.carro = null;
		this.funcionario = null;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public long getIdManutencao() {
		return idManutencao;
	}

	public Calendar getDataAgendada() {
		return dataAgendada;
	}

	public String getTipoServico() {
		return tipoServico;
	}

	public String getMotivoManutencao() {
		return motivoManutencao;
	}
	
}

