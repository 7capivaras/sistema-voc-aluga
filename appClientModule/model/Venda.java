package model;

import java.util.Calendar;

public class Venda {
	
	private long idVenda;
	private Calendar dataVenda;
	private double valor;
	
	private Carro carro;
	private Cliente cliente;
	
	public Venda( 	long id,
			     	Calendar data,
			     	double valor )
	{
		this.idVenda = id;
		this.dataVenda = data;
		this.valor = valor;
		
		this.carro = null;
		this.cliente = null;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public long getIdVenda() {
		return idVenda;
	}

	public Calendar getDataVenda() {
		return dataVenda;
	}

	public double getValor() {
		return valor;
	}

}
