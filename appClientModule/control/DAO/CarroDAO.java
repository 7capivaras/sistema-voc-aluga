package control.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import control.ConnectionFactory;
import model.Carro;
import model.Util;

public class CarroDAO {

	public boolean verificaPlaca(String placa) {
		
		boolean verificado = false;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Carro where placa = ?");
			stmt.setString(1, placa.toUpperCase());
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				verificado = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return verificado;		
	}

	public ArrayList<Carro> getCarros(){
		
		ArrayList<Carro> carros = new ArrayList<Carro>();
		Carro carro = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Carro;");
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				carro = getCarro(resultSet);
				carros.add(carro);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(carros.size() == 0){
			return null;
		}
		else
			return carros;
	}
	
	public ArrayList<Carro> getCarrosVenda(){
		
		ArrayList<Carro> carros = new ArrayList<Carro>();
		Carro carro = null;
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Carro where (kmrodados >= 40000 or CURRENT_DATE >= dataaquisicao+365) and id not in(select carro_id from voce_aluga.venda);");
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				carro = getCarro(resultSet);
				carros.add(carro);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(carros.size() == 0){
			return null;
		}
		else
			return carros;
	}
	
	public static Carro getCarro(String placa) {
		
		Carro carro = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Carro where placa = ?");
			stmt.setString(1, placa.toUpperCase());
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				carro = getCarro(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return carro;
		
	}
	
	public static boolean salvarCarro(Carro carro){
		
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		
		try{
			if( carro.getId() == 0 ){
				stmt = con.prepareStatement("INSERT INTO voce_aluga.carro( "+
			            "chassi, dataaquisicao, descricao, kmrodados, marca, modelo, "+
			            "placa, ultimarevisao, valordiaria, agencia_id) "+
			            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

			}
			else{
				stmt = con.prepareStatement("UPDATE voce_aluga.carro "+
							"SET chassi=?, dataaquisicao=?, descricao=?, kmrodados=?, marca=?, "+
							"modelo=?, placa=?, ultimarevisao=?, valordiaria=?, agencia_id=? "+
							"WHERE id=?;");
				stmt.setLong(11, carro.getId());
			}
			
			stmt.setString(1, carro.getChassi());
			stmt.setDate(2, Util.calendarToSQL(carro.getDataAquisicao()) );
			stmt.setString(3, carro.getDescricao());
			stmt.setDouble(4, carro.getKmRodados());
			stmt.setString(5, carro.getMarca());
			stmt.setString(6, carro.getModelo());
			stmt.setString(7, carro.getPlaca());
			stmt.setDate(8, Util.calendarToSQL(carro.getUltimaRevisao()) );
			stmt.setDouble(9, carro.getValorDiaria());
			stmt.setLong(10, carro.getAgencia().getIdAgencia() );
			
			stmt.execute();
			
			con.close();
			return true;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public static Carro getCarroById(long id) {
		
		Carro carro = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Carro where id = ?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				carro = getCarro(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return carro;
	}

	private static Carro getCarro(ResultSet resultSet) throws SQLException {
		Carro carro;
		
		Calendar dataAquisicaoCal = Calendar.getInstance();
		dataAquisicaoCal.setTime(resultSet.getDate("dataAquisicao"));

		Calendar ultimaRevisaoCal = Calendar.getInstance();
		ultimaRevisaoCal.setTime(resultSet.getDate("ultimarevisao"));
		
		carro = new Carro(  resultSet.getLong("id"),
							resultSet.getString("chassi"),
							resultSet.getString("marca"),
							resultSet.getString("modelo"),
							resultSet.getString("descricao"),
							dataAquisicaoCal,
							resultSet.getDouble("kmrodados"),
							ultimaRevisaoCal,
							resultSet.getString("placa").toUpperCase(),
							resultSet.getDouble("valordiaria") );
		
		carro.setAgencia( AgenciaDAO.getAgenciaById(resultSet.getLong("agencia_id")));
		
		return carro;
	}

	public static Carro getCarroByChassi(String chassi) {
		Carro carro = null;
		Connection con = new ConnectionFactory().getConnection();
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement("select * from voce_aluga.Carro where chassi = ?");
			stmt.setString(1, chassi );
			ResultSet resultSet = stmt.executeQuery();
			
			while (resultSet.next()){
				carro = getCarro(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return carro;
	}

}
