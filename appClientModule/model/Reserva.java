package model;
import java.util.Calendar;


public class Reserva
{
	private long idReserva;
	private Calendar dataReserva;
	private Calendar dataRetirada;
	private Calendar validadeReserva;

	private Cliente cliente;
	private Carro carro;
	private Funcionario funcionario;
	
	public Reserva(	long id,
					Calendar dataReserva,
					Calendar dataRetirada,
					Calendar validadeReserva )
	{
		this.idReserva = id;
		this.dataReserva = dataReserva;
		this.dataRetirada = dataRetirada;
		this.validadeReserva = validadeReserva;
		
		this.cliente = null;
		this.carro = null;
		this.funcionario = null;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public long getIdReserva() {
		return idReserva;
	}

	public Calendar getDataReserva() {
		return dataReserva;
	}

	public Calendar getDataRetirada() {
		return dataRetirada;
	}

	public Calendar getValidadeReserva() {
		return validadeReserva;
	}
	
}

